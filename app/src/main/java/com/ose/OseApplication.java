package com.ose;

import com.facebook.stetho.Stetho;
import com.ose.injection.component.ApplicationComponent;
import com.ose.injection.component.DaggerApplicationComponent;
import com.ose.injection.module.ApplicationModule;

import android.app.Application;
import android.content.Context;

import timber.log.Timber;

public class OseApplication extends Application {

    ApplicationComponent mApplicationComponent;

    public static OseApplication get(Context context) {
        return (OseApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
            Stetho.initializeWithDefaults(this);
        }
    }

    public ApplicationComponent getComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return mApplicationComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }
}
