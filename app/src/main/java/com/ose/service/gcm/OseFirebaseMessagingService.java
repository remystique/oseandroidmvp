package com.ose.service.gcm;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.ose.OseApplication;
import com.ose.R;
import com.ose.data.local.TchatPreferencesHelper;
import com.ose.data.model.interaction.NotificationAnswer;
import com.ose.data.model.stringdef.NotificationScreen;
import com.ose.injection.component.DaggerServiceComponent;
import com.ose.injection.module.ServiceModule;
import com.ose.ui.home.HomeActivity;
import com.ose.ui.thread.ThreadDiscussionActivity;

import org.joda.time.DateTime;

import java.util.List;

import javax.inject.Inject;

import dagger.Lazy;

public class OseFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Inject
    Lazy<TchatPreferencesHelper> mTchatPreferencesHelper;

    @Override
    public void onCreate() {
        super.onCreate();

        DaggerServiceComponent.builder()
                .serviceModule(new ServiceModule(this))
                .applicationComponent(OseApplication.get(this).getComponent())
                .build().inject(this);
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ


        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

            Gson gson = new GsonBuilder().registerTypeAdapter(DateTime.class,
                    (JsonDeserializer<DateTime>) (json, typeOfT, context) -> new DateTime(json.getAsString()))
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();


            try {
                for (String data : remoteMessage.getData().values()) {
                    NotificationAnswer notificationAnswer = gson.fromJson(data, NotificationAnswer.class);
                    sendNotification(notificationAnswer);
                }
            } catch (Exception e) {
                Log.e(TAG, "onMessageReceived: notif failed");
                // do nothing
            }

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                //scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]


    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param notificationAnswer FCM message body received.
     */
    private void sendNotification(NotificationAnswer notificationAnswer) {
        TchatPreferencesHelper tchatPreferencesHelper = mTchatPreferencesHelper.get();
        if (tchatPreferencesHelper != null) {
            if (ThreadDiscussionActivity.class.getName().equals(tchatPreferencesHelper.getTchatScreenName())
                    && !tchatPreferencesHelper.isScreenPaused()
                    && tchatPreferencesHelper.getTchatThreadId() != null
                    &&tchatPreferencesHelper.getTchatThreadId().equals(notificationAnswer.getInfo().getNotificationConversationContent().getThreadId())) {
                Intent intent = new Intent(this, ThreadDiscussionActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra(ThreadDiscussionActivity.THREAD_ID, notificationAnswer.getInfo().getNotificationConversationContent().getThreadId());
                getApplicationContext().startActivity(intent);
            } else {
                Intent intent = new Intent(this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if (NotificationScreen.CONVERSATION.equals(notificationAnswer.getInfo().getScreen())) {
                    intent.putExtra(HomeActivity.NOTIFICATION_THREAD_ID, notificationAnswer.getInfo().getNotificationConversationContent().getThreadId());
                }
                sendNotif(notificationAnswer, intent);
            }
        }
    }

    public NotificationManager getNotificationManager() {
        return (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    private void sendNotif(NotificationAnswer notificationAnswer, Intent intent) {
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = "channel_ose";
        String channelName = "Channel Ose";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);
            getNotificationManager().createNotificationChannel(mChannel);
        }

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.question_separator)
                        .setContentTitle(notificationAnswer.getAlertTitle())
                        .setContentText(notificationAnswer.getAlertContent())
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationAnswer.getAlertContent()))
                        .setSound(defaultSoundUri)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent);

        getNotificationManager().notify(0, notificationBuilder.build());
    }

    private boolean isDiscussionActive() throws ClassCastException {
        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        return taskInfo.get(0).topActivity.getClassName().equals(ThreadDiscussionActivity.class.getName());
    }
}