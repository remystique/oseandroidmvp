package com.ose.ui.profile;


import com.ose.data.model.user.User;
import com.ose.ui.base.MvpView;

import java.io.File;

public interface ProfileMvpView extends MvpView {

    void initView(User user);

    void setupPicture(File imageFile);

    void onUserUpdated();

    void onUserPictureUpdated();

    void onCloseProfile();
}
