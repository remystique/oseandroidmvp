package com.ose.ui.profile.menu;

import android.util.Log;

import com.ose.R;
import com.ose.data.ErrorManager;
import com.ose.data.UserManager;
import com.ose.data.model.user.User;
import com.ose.data.model.user.UserAnswer;
import com.ose.ui.base.BasePresenter;
import com.ose.ui.common.AbstractSubscriber;
import com.ose.ui.login.step1.LoginMvpView;

import java.net.HttpURLConnection;

import javax.inject.Inject;

import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ProfileMenuPresenter extends BasePresenter<ProfileMenuMvpView> {


    UserManager mUserManager;
    ErrorManager mErrorManager;

    private Subscription mSubscription;

    @Inject
    public ProfileMenuPresenter(UserManager userManager, ErrorManager errorManager) {
        mUserManager = userManager;
        mErrorManager = errorManager;
    }

    @Override
    public void attachView(ProfileMenuMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void initView() {
        final ProfileMenuMvpView profileMenuMvpView = getMvpView();
        profileMenuMvpView.showLoader();
        mSubscription = mUserManager.user()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new AbstractSubscriber<User, ProfileMenuMvpView>(mErrorManager, getMvpView()) {

                    @Override
                    public void onNext(User user) {
                        profileMenuMvpView.stopLoader();
                        profileMenuMvpView.initView(user);
                    }
                });
    }

    public void disconnect() {
        mUserManager.clearUserInformations();
        getMvpView().disconnect(R.string.error_message_disconnect);
    }
}
