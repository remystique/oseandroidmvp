package com.ose.ui.profile.menu;


import com.ose.data.model.user.User;
import com.ose.ui.base.MvpView;

public interface ProfileMenuMvpView extends MvpView {

    void initView(User user);
}
