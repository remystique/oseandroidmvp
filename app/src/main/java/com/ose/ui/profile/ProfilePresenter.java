package com.ose.ui.profile;

import android.text.TextUtils;
import android.util.Log;

import com.ose.data.ErrorManager;
import com.ose.data.UserManager;
import com.ose.data.model.user.User;
import com.ose.data.model.user.post.RegisterBody;
import com.ose.ui.base.BasePresenter;
import com.ose.ui.common.AbstractSubscriber;
import com.ose.ui.profile.menu.ProfileMenuMvpView;

import java.io.File;
import java.net.HttpURLConnection;

import javax.inject.Inject;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ProfilePresenter extends BasePresenter<ProfileMvpView> {


    UserManager mUserManager;
    ErrorManager mErrorManager;

    private Subscription mSubscription;

    @Inject
    public ProfilePresenter(UserManager userManager, ErrorManager errorManager) {
        mUserManager = userManager;
        mErrorManager = errorManager;
    }

    @Override
    public void attachView(ProfileMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void initView(User user) {
        getMvpView().initView(user);
    }

    public void setupPicture(File imageFile) {
        getMvpView().setupPicture(imageFile);
    }

    public void updateUser(RegisterBody registerBody, String picture) {
        final ProfileMvpView profileMvpView = getMvpView();
        profileMvpView.showLoader();
        mSubscription = mUserManager.updateUser(registerBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new AbstractSubscriber<String, ProfileMvpView>(mErrorManager, getMvpView()) {

                    @Override
                    public void onNext(String answer) {
                        profileMvpView.onUserUpdated();
                        if (!TextUtils.isEmpty(picture)) {
                            mSubscription = mUserManager.updateUserPicture(picture)
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribeOn(Schedulers.io())
                                    .subscribe(new AbstractSubscriber<String, ProfileMvpView>(mErrorManager, getMvpView()) {
                                        @Override
                                        public void onNext(String answer) {
                                            profileMvpView.stopLoader();
                                            profileMvpView.onUserPictureUpdated();
                                            profileMvpView.onCloseProfile();
                                        }
                                    });
                        } else {
                            profileMvpView.stopLoader();
                            profileMvpView.onCloseProfile();
                        }
                    }
                });
    }
}
