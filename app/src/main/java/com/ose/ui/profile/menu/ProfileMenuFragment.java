package com.ose.ui.profile.menu;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.ose.R;
import com.ose.data.model.user.User;
import com.ose.ui.base.BaseLoaderFragment;
import com.ose.ui.login.step1.LoginActivity;
import com.ose.ui.profile.ProfileActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileMenuFragment extends BaseLoaderFragment implements ProfileMenuMvpView {

    @Inject
    ProfileMenuPresenter profileMenuPresenter;

    @BindView(R.id.picture)
    ImageView mPicture;
    @BindView(R.id.name)
    TextView mName;

    @BindView(R.id.customize_profile)
    View mCustomizeProfile;

    @OnClick(R.id.disconnect)
    public void onDisconnect(View view) {
        profileMenuPresenter.disconnect();
    }

    @OnClick({R.id.facebook_button, R.id.twitter_button})
    public void onSocialButton(View view) {
        final String link;
        switch (view.getId()) {
            case R.id.facebook_button:
                link = "https://www.facebook.com/Oseapp/?fref=ts";
                break;
            case R.id.twitter_button:
            default:
                link = "https://twitter.com/oseapp";
                break;
        }
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        startActivity(browserIntent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        getActivityComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_profile_menu, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        profileMenuPresenter.attachView(this);
        profileMenuPresenter.initView();
    }

    @Override
    public void onResume() {
        super.onResume();
        profileMenuPresenter.initView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        profileMenuPresenter.detachView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        profileMenuPresenter = null;
    }

    @Override
    public void initView(User user) {
        mName.setText(String.format("%s %s", user.getFirstName(), user.getLastName()));
        Glide.with(getContext()).load(user.getPicture()).asBitmap().centerCrop().into(new BitmapImageViewTarget(mPicture) {
            @Override
            protected void setResource(Bitmap resource) {
                if (ProfileMenuFragment.this.isAdded()) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    mPicture.setImageDrawable(circularBitmapDrawable);
                }
            }
        });
        mCustomizeProfile.setOnClickListener(v -> ProfileActivity.start(getContext(), user));
    }
}
