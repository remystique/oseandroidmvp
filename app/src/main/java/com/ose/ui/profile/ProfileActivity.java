package com.ose.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.ose.R;
import com.ose.data.model.user.User;
import com.ose.ui.base.BaseLoaderActivity;
import com.ose.ui.common.widget.TakePictureView;
import com.ose.ui.common.widget.UserProfileView;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class ProfileActivity extends BaseLoaderActivity implements ProfileMvpView {

    private static final String USER = "user";
    @Inject
    ProfilePresenter mProfilePresenter;

    @BindView(R.id.user_profile_view)
    UserProfileView mUserProfileView;

    @BindView(R.id.profile_picture)
    TakePictureView mProfilePicture;

    public static Intent intent(final Context context, User user) {
        final Intent intent = new Intent(context, ProfileActivity.class);
        intent.putExtra(USER, user);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    public static void start(final Context context, User user) {
        context.startActivity(intent(context, user));
    }


    @OnClick(R.id.profile_picture)
    void onGetPictureClick() {
        EasyImage.openChooserWithGallery(this, "choose picture", 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        mProfilePresenter.attachView(this);
        final User user = (User) getIntent().getSerializableExtra(USER);
        mProfilePresenter.initView(user);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mUserProfileView.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Toast.makeText(ProfileActivity.this, "error", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                mProfilePresenter.setupPicture(imageFile);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mProfilePresenter.detachView();
    }

    @Override
    public void initView(User user) {
        setupToolbar();
        mUserProfileView.setupView(true, true);
        mUserProfileView.setupUser(user);
    }


    @Override
    public void setupPicture(File imageFile) {
        mProfilePicture.setImage(imageFile);
    }

    @Override
    public void onUserUpdated() {
        Toast.makeText(this, getString(R.string.profil_user_updated), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUserPictureUpdated() {
        Toast.makeText(this, getString(R.string.profil_user_picture_updated), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCloseProfile() {
        onBackPressed();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_profile:
                if (mUserProfileView.fieldsAreValid()) {
                    mProfilePresenter.updateUser(mUserProfileView.getRegisterBody(), mProfilePicture.getBase64Image());
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
