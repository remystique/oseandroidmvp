package com.ose.ui.common.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ose.R;
import com.ose.ui.common.ProgressionViewDrawable;

import org.joda.time.DateTime;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * ose
 * <p>
 * Created by remybarbosa on 04/02/2017.
 */
public class DayProgressionView extends LinearLayout {

    @BindView(R.id.progression_view)
    ImageView mProgressionView;

    @BindView(R.id.day_from)
    TextView mDayFrom;

    @BindView(R.id.day_to)
    TextView mDayTo;

    public DayProgressionView(Context context) {
        super(context);
        init(context);
    }

    public DayProgressionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DayProgressionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.view_day_progression, this);
        ButterKnife.bind(this);
    }

    public void setupView(int currentQuestionIndex) {
        final DateTime now = DateTime.now();
        mDayFrom.setText(now.toString("EEEE"));
        mDayTo.setText(now.plusDays(1).toString("EEEE"));
        mDayTo.post(() -> initProgressView(currentQuestionIndex));
    }

    private void initProgressView(int currentQuestionIndex) {
        mProgressionView.setImageDrawable(
                new ProgressionViewDrawable(
                        getContext(), currentQuestionIndex, mProgressionView.getWidth(), mProgressionView.getHeight()
                ));
    }
}

