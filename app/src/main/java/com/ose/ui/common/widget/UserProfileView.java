package com.ose.ui.common.widget;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.tasks.Task;

import com.ose.R;
import com.ose.data.model.intdef.Gender;
import com.ose.data.model.intdef.Interest;
import com.ose.data.model.user.User;
import com.ose.data.model.user.post.RegisterBody;
import com.ose.ui.common.DatePickerFragment;
import com.ose.utils.ViewUtil;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.content.res.AppCompatResources;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.ose.ui.register.step1.RegisterStep1Activity.RATIO;

/**
 * ose
 * <p>
 * Created by remybarbosa on 04/02/2017.
 */
public class UserProfileView extends LinearLayout {

    private static final int PLACES_REQUEST_CODE = 1001;

    @BindView(R.id.gender_selector)
    TextView mGenderSelector;

    @BindView(R.id.gender_man)
    TextView mGenderMan;

    @BindView(R.id.gender_woman)
    TextView mGenderWoman;

    @BindView(R.id.search_man)
    TextView mSearchMan;

    @BindView(R.id.search_woman)
    TextView mSearchWoman;

    @BindView(R.id.search_both)
    TextView mSearchBoth;

    @BindView(R.id.first_name)
    TextInputLayout mFirstName;

    @BindView(R.id.last_name)
    TextInputLayout mLastName;

    @BindView(R.id.birthday)
    TextInputLayout mBirthday;

    @BindView(R.id.password)
    TextInputLayout mPassword;

    @BindView(R.id.auto_complete_country)
    TextInputLayout mAutoCompleteCountry;

    @BindView(R.id.relation_wanted)
    LinearLayout mRelationWanted;


    @Interest
    int mInterest;

    @Gender
    int mGender;

    private boolean mGenderSelected;

    private Listener mListener;

    private DateTime mBirthDate;

    private double mLongitude;

    private double mLatitude;

    private String mCity;

    public UserProfileView(Context context) {
        super(context);
        init(context);
    }

    public UserProfileView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public UserProfileView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    private void init(Context context) {
        inflate(context, R.layout.user_profile, this);
        ButterKnife.bind(this);
        this.setOrientation(VERTICAL);
    }

    public void setupView(boolean enablePassword, boolean enableAutoComplete) {
        setupRelationWantedLayout();
        setupBirthdayListener();
        setupGenderBackgrounds();
        setupAutoCompleteCountry(enableAutoComplete);
        setupPassword(enablePassword);
    }

    private void deleteIt() {
        mFirstName.getEditText().setText("test");
        mLastName.getEditText().setText("test");
        mBirthday.getEditText().setText("test");
        mBirthDate = DateTime.now();
        mPassword.getEditText().setText("test");
    }

    private void setupPassword(boolean enablePassword) {
        mPassword.setVisibility(enablePassword ? VISIBLE : GONE);
    }

    private void setupAutoCompleteCountry(boolean enableAutoComplete) {
        if (enableAutoComplete) {
            mAutoCompleteCountry.setVisibility(VISIBLE);
            setAutoCompleteCountryListener();
        } else {
            mAutoCompleteCountry.setVisibility(GONE);
            final FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
            @SuppressLint("MissingPermission") final Task<Location> lastLocation = fusedLocationProviderClient.getLastLocation();
            lastLocation.addOnCompleteListener(locationTask -> {
                final Location location = locationTask.getResult();
                if (location != null) {
                    mLatitude = location.getLatitude();
                    mLongitude = location.getLongitude();

                    Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(mLatitude, mLongitude, 1);
                        for (Address address : addresses) {
                            mCity = address.getLocality() + ", " + address.getCountryName();
                        }
                    } catch (IOException e) {
                        // do nothing
                    }
                } else {
                    mAutoCompleteCountry.setVisibility(VISIBLE);
                    setAutoCompleteCountryListener();
                }
            });
        }
    }

    private void setAutoCompleteCountryListener() {
        final OnTouchListener touch = new OnTouchListener() {
            boolean down = false;

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    down = true;
                }
                if (down && event.getAction() == MotionEvent.ACTION_UP) {
                    openPlaces();
                    down = false;
                }
                return false;
            }
        };
        mAutoCompleteCountry.setOnTouchListener(touch);
        mAutoCompleteCountry.getEditText().setOnTouchListener(touch);
    }

    private void openPlaces() {
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .setFilter(new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES).build())
                    .build((Activity) getContext());
            ((Activity) getContext()).startActivityForResult(intent, PLACES_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            // do nothing
        }
    }

    private void setupGenderBackgrounds() {
        mGenderMan.setBackground(AppCompatResources.getDrawable(getContext(), R.drawable.blank_radio_btn));
        mGenderWoman.setBackground(AppCompatResources.getDrawable(getContext(), R.drawable.blank_radio_btn));
    }

    private void setupRelationWantedLayout() {
        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewUtil.getScreenWidth(getContext()) / RATIO);
        mRelationWanted.setLayoutParams(params);
    }


    @OnClick({R.id.search_man, R.id.search_woman, R.id.search_both})
    void onWantedClick(View view) {
        if (fieldsAreValid()) {
            switch (view.getId()) {
                case R.id.search_man:
                    mInterest = Interest.MAN;
                    break;
                case R.id.search_woman:
                    mInterest = Interest.WOMAN;
                    break;
                case R.id.search_both:
                    mInterest = Interest.BOTH;
                    break;
                default:
                    mInterest = Interest.BOTH;
            }

            mSearchMan.setBackgroundResource(R.drawable.blank_round_btn);
            mSearchWoman.setBackgroundResource(R.drawable.blank_round_btn);
            mSearchBoth.setBackgroundResource(R.drawable.blank_round_btn);
            view.setBackground(AppCompatResources.getDrawable(getContext(), R.drawable.yellow_round_btn));

            if (mListener != null) {
                mListener.onCompletedProfile(getRegisterBody());
            }
        }
    }

    public boolean fieldsAreValid() {
        mGenderSelector.setError(null);
        mFirstName.setError(null);
        mLastName.setError(null);
        mBirthday.setError(null);
        mPassword.setError(null);

        if (!mGenderSelected) {
            mGenderSelector.setError(getContext().getString(R.string.gender_needed));
            return false;
        }
        if (TextUtils.isEmpty(mFirstName.getEditText().getText())) {
            mFirstName.setError(getContext().getString(R.string.firstname_needed));
            return false;
        }
        if (TextUtils.isEmpty(mLastName.getEditText().getText())) {
            mLastName.setError(getContext().getString(R.string.lastname_needed));
            return false;
        }
        if (TextUtils.isEmpty(mBirthday.getEditText().getText())) {
            mBirthday.setError(getContext().getString(R.string.birthday_needed));
            return false;
        }

        return true;
    }

    @OnClick({R.id.gender_man, R.id.gender_woman})
    void onGenderClick(View view) {
        mGenderMan.setBackground(AppCompatResources.getDrawable(getContext(), R.drawable.blank_radio_btn));
        mGenderWoman.setBackground(AppCompatResources.getDrawable(getContext(), R.drawable.blank_radio_btn));
        view.setBackground(AppCompatResources.getDrawable(getContext(), R.drawable.yellow_radio_btn));

        switch (view.getId()) {
            case R.id.gender_man:
                mGender = Gender.MALE;
                break;
            case R.id.gender_woman:
                mGender = Gender.FEMALE;
                break;
            default:
                mGender = Gender.MALE;
                break;
        }

        mGenderSelected = true;
    }


    private void setupBirthdayListener() {
        final View.OnTouchListener touch = new View.OnTouchListener() {
            boolean down = false;

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    down = true;
                }
                if (down && event.getAction() == MotionEvent.ACTION_UP) {
                    getDate();
                    down = false;
                }
                return false;
            }
        };
        mBirthday.setOnTouchListener(touch);
        mBirthday.getEditText().setOnTouchListener(touch);
    }

    private void getDate() {
        final DatePickerFragment datePickerFragment = DatePickerFragment.newInstance();
        datePickerFragment.setListener(date -> {
            mBirthDate = date.toDateTimeAtCurrentTime();
            mBirthday.getEditText().setText(date.toString("dd/MM/yyyy"));
        });
        AppCompatActivity appCompatActivity = (AppCompatActivity) getContext();
        datePickerFragment.show(appCompatActivity.getSupportFragmentManager());
    }

    public RegisterBody getRegisterBody() {
        final String password = mPassword.getEditText().getText().toString();
        return RegisterBody.builder()
                .firstName(String.valueOf(mFirstName.getEditText().getText()))
                .lastName(String.valueOf(mLastName.getEditText().getText()))
                .interest(mInterest)
                .birthDate(mBirthDate.toString("yyyy-MM-dd"))
                .gender(mGender)
                .password(TextUtils.isEmpty(password) ? null : password)
                .lat(mLatitude)
                .lng(mLongitude)
                .city(mCity)
                .build();
    }

    public void setupUser(User user) {
        mFirstName.getEditText().setText(user.getFirstName());
        mLastName.getEditText().setText(user.getLastName());
        mBirthday.getEditText().setText(user.getBirthDate());
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        mBirthDate = formatter.parseDateTime(user.getBirthDate());

        mCity = user.getCity();
        mLatitude = user.getLat();
        mLongitude = user.getLng();
        mAutoCompleteCountry.getEditText().setText(user.getCity());

        mGender = user.getGender();
        if (user.getGender() == Gender.MALE) {
            mGenderMan.setBackground(AppCompatResources.getDrawable(getContext(), R.drawable.yellow_radio_btn));
        } else {
            mGenderWoman.setBackground(AppCompatResources.getDrawable(getContext(), R.drawable.yellow_radio_btn));
        }
        mGenderSelected = true;

        mInterest = user.getInterest();
        switch (user.getInterest()) {
            case Interest.BOTH:
                mSearchBoth.setBackground(AppCompatResources.getDrawable(getContext(), R.drawable.yellow_round_btn));
                break;
            case Interest.MAN:
                mSearchMan.setBackground(AppCompatResources.getDrawable(getContext(), R.drawable.yellow_round_btn));
                break;
            case Interest.WOMAN:
                mSearchWoman.setBackground(AppCompatResources.getDrawable(getContext(), R.drawable.yellow_round_btn));
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACES_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getContext(), data);
                mLatitude = place.getLatLng().latitude;
                mLongitude = place.getLatLng().longitude;
                mCity = place.getAddress().toString();
                mAutoCompleteCountry.getEditText().setText(mCity);

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            } else if (resultCode == RESULT_CANCELED) {
            }
        }
    }

    public interface Listener {

        void onCompletedProfile(RegisterBody registerBody);
    }

}
