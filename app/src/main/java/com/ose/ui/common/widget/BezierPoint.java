package com.ose.ui.common.widget;

import android.graphics.PointF;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 26/02/2017.
 */
@Builder
@Value
public class BezierPoint {
    private PointF point;
    private PointF firstControlPoint;
    private PointF secondControlPoint;
}
