package com.ose.ui.common.widget;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 16/02/2017.
 */
@Value
@Builder
public class GooglePlace {
    private String description;
    private String id;
    private GoogleLocation googleLocation;
}
