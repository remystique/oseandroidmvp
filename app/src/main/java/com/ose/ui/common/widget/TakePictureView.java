package com.ose.ui.common.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.AttributeSet;
import android.util.Base64;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import java.io.ByteArrayOutputStream;
import java.io.File;

/**
 * ose
 * <p>
 * Created by remybarbosa on 04/02/2017.
 */
public class TakePictureView extends ImageView {

    public static final int IMAGE_SIZE_MAX = 512;
    private File mImageFile;

    public TakePictureView(Context context) {
        super(context);
    }

    public TakePictureView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TakePictureView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setImage(File imageFile) {
        mImageFile = imageFile;
        Glide.with(getContext()).load(imageFile).asBitmap().centerCrop().into(new BitmapImageViewTarget(this) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                TakePictureView.this.setImageDrawable(circularBitmapDrawable);
            }
        });
    }

    public String getBase64Image() {
        if (mImageFile != null) {
            Bitmap bm = getBitmap();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] byteArrayImage = byteArrayOutputStream.toByteArray();
            return Base64.encodeToString(byteArrayImage, Base64.NO_WRAP);
        }
        return null;
    }

    private Bitmap getBitmap() {
        Bitmap bm = BitmapFactory.decodeFile(mImageFile.getAbsolutePath());
        float imageRatio = (float) bm.getWidth() / bm.getHeight();
        bm = Bitmap.createScaledBitmap(bm, (int) (IMAGE_SIZE_MAX * imageRatio), IMAGE_SIZE_MAX, true);
        return bm;
    }
}
