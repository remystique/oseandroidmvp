package com.ose.ui.common.widget;

import com.ose.R;
import com.ose.data.model.intdef.ProgressionKind;
import com.ose.utils.DeviceUtil;

import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * ose
 * <p>
 * Created by remybarbosa on 04/02/2017.
 */
public class WeekProgressionView extends View {

    public static final int RATE = 2;
    int mCurStep = 0;
    private List<BezierPoint> bezierPoints = new ArrayList<>(Arrays.asList(
            BezierPoint.builder().point(new PointF(177.18f, 158.71f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(184.59f, 147.13f)).secondControlPoint(new PointF(181.26f, 153.11f)).point(new PointF(187.17f, 140.92f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(191.53f, 128.44f)).secondControlPoint(new PointF(189.72f, 134.79f)).point(new PointF(192.63f, 121.99f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(194.09f, 108.8f)).secondControlPoint(new PointF(193.73f, 115.44f)).point(new PointF(193.7f, 102.18f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(192.18f, 89.01f)).secondControlPoint(new PointF(193.31f, 95.58f)).point(new PointF(190.3f, 82.62f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(185.99f, 70.47f)).secondControlPoint(new PointF(188.49f, 76.47f)).point(new PointF(182.8f, 64.76f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(175.87f, 53.82f)).secondControlPoint(new PointF(179.67f, 59.15f)).point(new PointF(171.4f, 48.87f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(160.78f, 38.71f)).secondControlPoint(new PointF(170.41f, 47.77f)).point(new PointF(156.69f, 35.83f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(145.61f, 28.96f)).secondControlPoint(new PointF(151.31f, 32.06f)).point(new PointF(139.7f, 26.55f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(127.37f, 22.31f)).secondControlPoint(new PointF(133.64f, 24.07f)).point(new PointF(120.99f, 21.26f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(107.97f, 19.85f)).secondControlPoint(new PointF(114.53f, 20.19f)).point(new PointF(101.45f, 20.25f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(88.51f, 21.76f)).secondControlPoint(new PointF(94.96f, 20.64f)).point(new PointF(82.23f, 23.6f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(70.06f, 27.91f)).secondControlPoint(new PointF(76.06f, 25.4f)).point(new PointF(64.33f, 31.11f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(53.4f, 38.04f)).secondControlPoint(new PointF(58.73f, 34.24f)).point(new PointF(48.46f, 42.5f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(38.48f, 52.92f)).secondControlPoint(new PointF(47.34f, 43.51f)).point(new PointF(35.69f, 56.85f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(28.53f, 68.32f)).secondControlPoint(new PointF(31.75f, 62.41f)).point(new PointF(26.04f, 74.47f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(21.9f, 86.56f)).secondControlPoint(new PointF(23.63f, 80.41f)).point(new PointF(20.85f, 92.8f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(19.4f, 105.98f)).secondControlPoint(new PointF(19.76f, 99.34f)).point(new PointF(19.8f, 112.58f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(21.3f, 125.56f)).secondControlPoint(new PointF(20.19f, 119.09f)).point(new PointF(23.13f, 131.86f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(27.49f, 144.21f)).secondControlPoint(new PointF(24.95f, 138.12f)).point(new PointF(30.74f, 150.01f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(37.68f, 160.95f)).secondControlPoint(new PointF(33.88f, 155.62f)).point(new PointF(42.15f, 165.89f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(52.59f, 176.04f)).secondControlPoint(new PointF(43.12f, 166.96f)).point(new PointF(56.3f, 179.54f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(65.84f, 188.51f)).secondControlPoint(new PointF(61.07f, 184.02f)).point(new PointF(70.61f, 193f)).build(),
            BezierPoint.builder().point(new PointF(84.8f, 206.35f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(94.35f, 215.34f)).secondControlPoint(new PointF(89.57f, 210.84f)).point(new PointF(99.13f, 219.83f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(108.12f, 228.29f)).secondControlPoint(new PointF(103.26f, 223.71f)).point(new PointF(113.33f, 233.2f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(122.77f, 242.07f)).secondControlPoint(new PointF(118.05f, 237.64f)).point(new PointF(127.49f, 246.51f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(136.79f, 255.27f)).secondControlPoint(new PointF(131.54f, 250.32f)).point(new PointF(142.18f, 260.34f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(151.65f, 269.25f)).secondControlPoint(new PointF(146.92f, 264.79f)).point(new PointF(156.39f, 273.71f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(169.83f, 286.47f)).secondControlPoint(new PointF(160.38f, 277.46f)).point(new PointF(170.55f, 287.24f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(179.11f, 297.61f)).secondControlPoint(new PointF(175.17f, 292.22f)).point(new PointF(182.36f, 303.28f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(188.14f, 314.86f)).secondControlPoint(new PointF(185.59f, 308.93f)).point(new PointF(190.01f, 320.96f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(193.19f, 333.98f)).secondControlPoint(new PointF(191.98f, 327.38f)).point(new PointF(193.65f, 340.62f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(193.81f, 353.93f)).secondControlPoint(new PointF(194.1f, 347.25f)).point(new PointF(192.76f, 360.51f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(189.96f, 373.37f)).secondControlPoint(new PointF(191.72f, 366.98f)).point(new PointF(187.46f, 379.52f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(181.89f, 391.33f)).secondControlPoint(new PointF(185.03f, 385.53f)).point(new PointF(178.06f, 396.78f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(166.12f, 410.52f)).secondControlPoint(new PointF(175.21f, 400.84f)).point(new PointF(164.98f, 411.55f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(154.73f, 419.78f)).secondControlPoint(new PointF(160.05f, 416f)).point(new PointF(149.15f, 422.9f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(137.19f, 428.7f)).secondControlPoint(new PointF(143.31f, 426.16f)).point(new PointF(130.89f, 430.51f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(117.92f, 433.45f)).secondControlPoint(new PointF(124.49f, 432.35f)).point(new PointF(111.32f, 433.79f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(98.01f, 433.73f)).secondControlPoint(new PointF(104.68f, 434.14f)).point(new PointF(91.44f, 432.56f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(78.58f, 429.52f)).secondControlPoint(new PointF(84.96f, 431.41f)).point(new PointF(72.44f, 426.89f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(60.88f, 421.16f)).secondControlPoint(new PointF(66.55f, 424.36f)).point(new PointF(55.56f, 417.27f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(42.23f, 405.32f)).secondControlPoint(new PointF(51.92f, 414.62f)).point(new PointF(40.87f, 403.74f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(33.86f, 394.79f)).secondControlPoint(new PointF(37.11f, 399.41f)).point(new PointF(31.1f, 389.96f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(24.56f, 376.55f)).secondControlPoint(new PointF(27.38f, 383.44f)).point(new PointF(22.66f, 369.46f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(19.93f, 356.39f)).secondControlPoint(new PointF(20.92f, 363.01f)).point(new PointF(19.7f, 349.75f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(19.95f, 336.9f)).secondControlPoint(new PointF(19.47f, 343.33f)).point(new PointF(21.13f, 330.57f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(24.2f, 317.97f)).secondControlPoint(new PointF(22.31f, 324.21f)).point(new PointF(26.8f, 311.95f)).build(),
            BezierPoint.builder().firstControlPoint(new PointF(32.48f, 300.55f)).secondControlPoint(new PointF(29.31f, 306.14f)).point(new PointF(36.31f, 295.29f)).build()
    ));
    private int mProgression = 0;
    private List<Integer> mProgressions;

    public WeekProgressionView(Context context) {
        super(context);
    }

    public WeekProgressionView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WeekProgressionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public static RectF resizingBehaviorApply(ResizingBehavior behavior, RectF rect, RectF target) {
        if (rect.equals(target) || target == null) {
            return rect;
        }

        if (behavior == ResizingBehavior.STRETCH) {
            return target;
        }

        PointF ratio = new PointF();
        ratio.x = Math.abs(target.width() / rect.width());
        ratio.y = Math.abs(target.height() / rect.height());

        float scale = 0f;

        switch (behavior) {
            case ASPECT_FIT: {
                scale = Math.min(ratio.x, ratio.y);
                break;
            }
            case ASPECT_FILL: {
                scale = Math.max(ratio.x, ratio.y);
                break;
            }
            case CENTER: {
                scale = 1f;
                break;
            }
        }

        PointF newSize = new PointF(Math.abs(rect.width() * scale), Math.abs(rect.height() * scale));
        RectF result = new RectF(target.centerX(), target.centerY(), target.centerX(), target.centerY());
        result.inset(-newSize.x / 2f, -newSize.y / 2f);
        return result;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawGroupCopyCanvas(canvas, new RectF(0f, 0f, getWidth(), getHeight()), ResizingBehavior.ASPECT_FIT);
    }

    public void drawGroupCopyCanvas(Canvas canvas, RectF targetFrame, ResizingBehavior resizing) {
        canvas.save();
        RectF resizedFrame = resizingBehaviorApply(resizing, new RectF(0f, 0f, 214f, 454f), targetFrame);
        canvas.translate(new PointF(resizedFrame.left, resizedFrame.top).x, new PointF(resizedFrame.left, resizedFrame.top).y);
        canvas.scale(resizedFrame.width() / 214f, resizedFrame.height() / 454f);

        drawBackground(canvas);
        drawBackgroundPoints(canvas);
        drawLabels(canvas);

        if (mProgression > 0) {
            drawProgression(canvas);
        }

//        canvas.restore();
    }

    private void drawLabels(Canvas canvas) {
        RectF labelRect = new RectF(127.13f, 185.97f, 164.76f, 203.97f);
        TextPaint labelTextPaint = new TextPaint();
        labelTextPaint.setColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        labelTextPaint.setTextSize(14.07f);
        LocalDate now = new LocalDate();
        LocalDate monday = now.withDayOfWeek(DateTimeConstants.MONDAY);
        StaticLayout labelStaticLayout = new StaticLayout(monday.toString("EE"), labelTextPaint, (int) labelRect.width(),
                Layout.Alignment.ALIGN_NORMAL, 1f, 0f, false);
        canvas.save();
        canvas.clipRect(labelRect);
        canvas.translate(labelRect.left, labelRect.top);
        labelStaticLayout.draw(canvas);
        canvas.restore();

        // Label 2
        RectF label2Rect = new RectF(47.18f, 249.61f, 96.29f, 267.61f);
        TextPaint label2TextPaint = new TextPaint();
        label2TextPaint.setColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
        label2TextPaint.setTextSize(14.07f);

        LocalDate friday = now.withDayOfWeek(DateTimeConstants.FRIDAY);
        StaticLayout label2StaticLayout = new StaticLayout(friday.toString("EE"), label2TextPaint, (int) label2Rect.width(),
                Layout.Alignment.ALIGN_NORMAL, 1f, 0f, false);
        canvas.save();
        canvas.clipRect(label2Rect);
        canvas.translate(label2Rect.left, label2Rect.top);
        label2StaticLayout.draw(canvas);
        canvas.restore();
    }

    private void drawBackground(Canvas canvas) {
        Path background = new Path();

        for (int i = 0; i < bezierPoints.size(); i++) {
            final BezierPoint bezierPoint = bezierPoints.get(i);
            if (i == 0) {
                background.moveTo(bezierPoint.getPoint().x, bezierPoint.getPoint().y);
                background.lineTo(bezierPoint.getPoint().x, bezierPoint.getPoint().y + 0.001f);
            } else {
                if (bezierPoint.getFirstControlPoint() == null) {
                    background.lineTo(bezierPoint.getPoint().x, bezierPoint.getPoint().y);
                } else {
                    background.cubicTo(bezierPoint.getFirstControlPoint().x, bezierPoint.getFirstControlPoint().y,
                            bezierPoint.getSecondControlPoint().x, bezierPoint.getSecondControlPoint().y,
                            bezierPoint.getPoint().x, bezierPoint.getPoint().y);
                }
            }
        }
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(39f);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        canvas.save();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
        canvas.drawPath(background, paint);
    }

    private void drawProgression(Canvas canvas) {
        Path progression = new Path();
        if (mCurStep / RATE < mProgression) {
            for (int i = 0; i < mCurStep / RATE; i++) {
                fillPath(progression, i);
            }
            drawAnimateProgression(canvas, progression);
            mCurStep++;
            invalidate();
        } else {
            for (int i = 0; i < mProgression && i < mProgressions.size(); i++) {
                fillPath(progression, i);
            }
            drawAnimateProgression(canvas, progression);
        }
    }

    private void drawAnimateProgression(Canvas canvas, Path progression) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(39f);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        canvas.save();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        canvas.drawPath(progression, paint);
        drawPoints(canvas);
    }

    private void fillPath(Path progression, int i) {
        final BezierPoint bezierPoint = bezierPoints.get(i);
        if (i == 0) {
            progression.moveTo(bezierPoint.getPoint().x, bezierPoint.getPoint().y);
            progression.lineTo(bezierPoint.getPoint().x, bezierPoint.getPoint().y + 0.001f);
        } else {
            if (bezierPoint.getFirstControlPoint() == null) {
                progression.lineTo(bezierPoint.getPoint().x, bezierPoint.getPoint().y);
            } else {
                progression.cubicTo(bezierPoint.getFirstControlPoint().x, bezierPoint.getFirstControlPoint().y,
                        bezierPoint.getSecondControlPoint().x, bezierPoint.getSecondControlPoint().y,
                        bezierPoint.getPoint().x, bezierPoint.getPoint().y);
            }
        }
    }

    private void drawBackgroundPoints(Canvas canvas) {
        Paint yellowPaint = new Paint() {
            {
                setStyle(Style.FILL);
                setColor(ContextCompat.getColor(getContext(), R.color.pointProgressionColor));
            }
        };
        for (int i = 0; i < bezierPoints.size(); i++) {
            final BezierPoint bezierPoint = bezierPoints.get(i);
            final int radius = getRadius(i + 1);
            canvas.drawCircle(bezierPoint.getPoint().x, bezierPoint.getPoint().y, radius, yellowPaint);
        }
    }

    private void drawPoints(Canvas canvas) {
        Paint redPaint = new Paint() {
            {
                setStyle(Style.FILL);
                setColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
            }
        };
        Paint greyPaint = new Paint() {
            {
                setStyle(Style.FILL);
                setColor(ContextCompat.getColor(getContext(), R.color.dark_grey));
            }
        };

        if (mCurStep / RATE < mProgression) {
            for (int i = 0; i < mCurStep / RATE; i++) {
                final BezierPoint bezierPoint = bezierPoints.get(i);
                final @ProgressionKind int progressionPoint = mProgressions.get(i);
                final int radius = getRadius(i + 1);
                if (progressionPoint == ProgressionKind.CANT_ANSWERED) {
                    canvas.drawCircle(bezierPoint.getPoint().x, bezierPoint.getPoint().y, radius, greyPaint);
                } else {
                    canvas.drawCircle(bezierPoint.getPoint().x, bezierPoint.getPoint().y, radius, redPaint);
                }
            }
        } else {
            for (int i = 0; i < mProgression && i < mProgressions.size(); i++) {
                final BezierPoint bezierPoint = bezierPoints.get(i);
                final @ProgressionKind int progressionPoint = mProgressions.get(i);
                final int radius = getRadius(i + 1);
                if (progressionPoint == ProgressionKind.CANT_ANSWERED) {
                    canvas.drawCircle(bezierPoint.getPoint().x, bezierPoint.getPoint().y, radius, greyPaint);
                } else {
                    canvas.drawCircle(bezierPoint.getPoint().x, bezierPoint.getPoint().y, radius, redPaint);
                }
            }
        }
    }

    private int getRadius(int position) {
        int radius;
        int normalRadius = DeviceUtil.convertDpToPixel(1, getContext());
        int dayPositionRadius = DeviceUtil.convertDpToPixel(2, getContext());
        if (position % 10 == 0 && position != 0) {
            radius = dayPositionRadius;
        } else {
            radius = normalRadius;
        }
        return radius;
    }

    public void setData(List<Integer> progressions) {
        mProgressions = progressions;
        mProgression = findProgression(progressions);
        invalidate();
    }

    private int findProgression(List<Integer> progressions) {
        for (int i = 0; i < progressions.size(); i++) {
            if (progressions.get(i).equals(ProgressionKind.NOT_ANSWERED)) {
                return i - 1;
            }
        }
        return bezierPoints.size();
    }

    public enum ResizingBehavior {
        ASPECT_FIT,
        ASPECT_FILL,
        STRETCH,
        CENTER,
    }

}

