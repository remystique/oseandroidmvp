package com.ose.ui.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;

import com.ose.R;
import com.ose.utils.DeviceUtil;

/**
 * Created by MacBook on 23/07/15.
 */
public class ProgressionViewDrawable extends Drawable {

    public static final int NUMBER_OF_POINT = 10;
    private final int mCurrentPosition;
    private final float mWidth;
    private final float mHeight;
    private final int mNormalRadius;
    private final int mCurrentPositionRadius;
    private final int mMargins;
    private int mStartColor;
    private int mEndColor;

    public ProgressionViewDrawable(Context context, int currentPosition, int width, int height) {
        mStartColor = ContextCompat.getColor(context, R.color.colorPrimary);
        mEndColor = ContextCompat.getColor(context, R.color.colorAccent);
        mCurrentPosition = currentPosition;
        mWidth = width;
        mHeight = height / 2;
        mNormalRadius = DeviceUtil.convertDpToPixel(3, context);
        mCurrentPositionRadius = DeviceUtil.convertDpToPixel(5, context);
        mMargins = DeviceUtil.convertDpToPixel(5, context);
    }

    @Override
    public void draw(Canvas canvas) {

        Paint paint = new Paint() {
            {
                setStyle(Style.FILL);
                setColor(mStartColor);
            }
        };

        Paint paintEnd = new Paint() {
            {
                setStyle(Style.FILL);
                setColor(mEndColor);
            }
        };

        float step = (mWidth - mMargins * 2) / (NUMBER_OF_POINT - 1);
        for (int i = 0; i < NUMBER_OF_POINT; i++) {
            final float xPosition = (step * i) + mMargins;
            if (i < mCurrentPosition) {
                canvas.drawCircle(xPosition, mHeight, mNormalRadius, paint);
            } else if (i == mCurrentPosition) {
                canvas.drawCircle(xPosition, mHeight, mCurrentPositionRadius, paint);
            } else {
                canvas.drawCircle(xPosition, mHeight, mNormalRadius, paintEnd);
            }
        }
    }


    @Override
    public void setAlpha(int alpha) {
        // Has no effect
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        // Has no effect
    }

    @Override
    public int getOpacity() {
        // Not Implemented
        return PixelFormat.UNKNOWN;
    }

}
