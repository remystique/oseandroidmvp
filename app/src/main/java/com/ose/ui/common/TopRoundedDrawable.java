package com.ose.ui.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PixelFormat;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;

import com.ose.R;
import com.ose.utils.DeviceUtil;

/**
 * Created by MacBook on 23/07/15.
 */
public class TopRoundedDrawable extends Drawable {

    private int mStartColor;
    private int mEndColor;
    private float mX1;
    private float mY1;
    private float mX2;
    private float mY2;
    private float mX3;
    private float mY3;

    public TopRoundedDrawable(int startColor, int endColor, Context context) {
        final float marge = context.getResources().getDimension(R.dimen.header_marge);
        final float height = context.getResources().getDimension(R.dimen.header_size);

        mStartColor = startColor;
        mEndColor = endColor;
        mX1 = 0;
        mY1 = marge;
        mX2 = DeviceUtil.getScreenWidth(context) / 2;
        mY2 = height + marge;
        mX3 = DeviceUtil.getScreenWidth(context);
        mY3 = marge;
    }

    @Override
    public void draw(Canvas canvas) {

        Paint paint = new Paint() {
            {
                setStyle(Paint.Style.FILL);
                setStrokeWidth(3.0f);
            }
        };
        paint.setShader(new LinearGradient(0, 0, 0, mY2, mStartColor, mEndColor, Shader.TileMode.MIRROR));

        final Path path = new Path();
        path.moveTo(mX1, mY1);
        path.quadTo(mX2, mY2, mX3, mY3);
        canvas.drawRect(mX1, mX1, mX3, mY1, paint);

        canvas.drawPath(path, paint);

    }


    @Override
    public void setAlpha(int alpha) {
        // Has no effect
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        // Has no effect
    }

    @Override
    public int getOpacity() {
        // Not Implemented
        return PixelFormat.UNKNOWN;
    }

}
