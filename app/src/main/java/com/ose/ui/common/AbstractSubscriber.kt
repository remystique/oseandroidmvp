package com.ose.ui.common

import android.util.Log
import com.ose.R
import com.ose.data.ErrorManager
import com.ose.ui.base.MvpView

import java.net.ConnectException
import java.net.HttpURLConnection

import retrofit2.adapter.rxjava.HttpException
import rx.Subscriber

abstract class AbstractSubscriber<T, V : MvpView>(private val mErrorManager: ErrorManager?, private val mMvpView: V) : Subscriber<T>() {

    override fun onCompleted() {

    }

    override fun onError(throwable: Throwable) {
        onError(throwable, { e ->
            mMvpView.stopLoader()
            Log.e("TAG", "" + e.toString())
            mMvpView.showError()
        })
    }

    fun onError(e: Throwable, onErrorAction : (e: Throwable) -> Unit) {
        if (e is HttpException && e.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
            mMvpView.stopLoader()
            mErrorManager?.clearUserInformations()
            mMvpView.disconnect()
        } else if (e is ConnectException && e.message?.contains(":8443") == true) {
            mMvpView.stopLoader()
            mMvpView.showError(R.string.try_without_wifi)
        } else {
            onErrorAction(e)
        }
    }



    override fun onNext(t: T) {

    }
}
