package com.ose.ui.common;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.widget.DatePicker;

import org.joda.time.LocalDate;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public static final int ADULT_AGE = 18;
    private boolean mIsStopping = false;
    private Listener mListener;

    public static DatePickerFragment newInstance() {
        return new DatePickerFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final LocalDate initialDate = LocalDate.now().minusYears(ADULT_AGE);

        final int datePickerYear = initialDate.getYear();
        final int datePickerMonth = initialDate.getMonthOfYear() - 1;
        final int datePickerDay = initialDate.getDayOfMonth();

        return new DatePickerDialog(getActivity(), DatePickerFragment.this, datePickerYear, datePickerMonth, datePickerDay) {
            @Override
            protected void onStop() {
                //Bug on DatePickerDialog cancel see http://stackoverflow.com/questions/11444238/jelly-bean-datepickerdialog-is-there-a-way-to-cancel/
                mIsStopping = true;
                super.onStop();
            }
        };
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        if (!mIsStopping) {
            final LocalDate selectedDate = new LocalDate(year, monthOfYear + 1, dayOfMonth);
            if (mListener != null) {
                mListener.onDateChanged(selectedDate);
            }
        }
    }

    public void show(final FragmentManager manager) {
        show(manager, DatePickerFragment.class.getName());
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }


    public interface Listener {
        void onDateChanged(LocalDate date);
    }
}
