package com.ose.ui.base;

import com.ose.data.BaseActivityManager;

import javax.inject.Inject;

public class BaseActivityPresenter extends BasePresenter<MvpView> {


    BaseActivityManager mBaseActivityManager;

    @Inject
    public BaseActivityPresenter(BaseActivityManager baseActivityManager) {
        mBaseActivityManager = baseActivityManager;
    }

    @Override
    public void attachView(MvpView mvpView) {
        super.attachView(mvpView);
    }

    public void saveScreenName(String screenName) {
        mBaseActivityManager.saveScreenName(screenName);
    }

    public void saveScreenPaused(boolean isPaused) {
        mBaseActivityManager.saveScreenPaused(isPaused);
    }
}
