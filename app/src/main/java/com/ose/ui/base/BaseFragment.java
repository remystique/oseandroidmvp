package com.ose.ui.base;

import android.support.v4.app.Fragment;

import com.ose.injection.component.ActivityComponent;


public class BaseFragment extends Fragment {

    private ActivityComponent mActivityComponent;

    protected ActivityComponent getActivityComponent() {
        return ((BaseActivity) getActivity()).getActivityComponent();
    }
}
