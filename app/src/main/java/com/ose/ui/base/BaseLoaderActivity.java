package com.ose.ui.base;

import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;

import com.ose.R;
import com.ose.ui.login.step1.LoginActivity;
import com.ose.utils.DialogFactory;

import butterknife.BindView;


public class BaseLoaderActivity extends BaseActivity implements MvpView {

    @BindView(R.id.loader)
    View mLoader;

    @Override
    public void showLoader() {
        mLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoader() {
        mLoader.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        showError(R.string.error_message);
    }

    @Override
    public void showError(int resId) {
        DialogFactory.createGenericErrorDialog(this, getString(resId)).show();
    }

    @Override
    public void disconnect() {
        disconnect(R.string.error_message_token_expired);
    }

    @Override
    public void disconnect(int resId) {
        DialogFactory.createGenericErrorDialog(this, getString(resId),
                (dialog, which) -> {
                    LoginActivity.start(BaseLoaderActivity.this);
                    finish();
                })
                .show();
    }
}
