package com.ose.ui.base;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.ose.R;
import com.ose.ui.login.step1.LoginActivity;
import com.ose.utils.DialogFactory;

import butterknife.BindView;


public class BaseLoaderFragment extends BaseFragment implements MvpView {
    @BindView(R.id.loader)
    View mLoader;

    @Override
    public void showLoader() {
        mLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoader() {

        mLoader.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        showError(R.string.error_message);
    }

    @Override
    public void showError(int resId) {
        if (isAdded()) {
            DialogFactory.createGenericErrorDialog(getContext(), getString(resId)).show();
        }
    }

    @Override
    public void disconnect() {
        disconnect(R.string.error_message_token_expired);
    }

    @Override
    public void disconnect(int resId) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            DialogFactory.createGenericErrorDialog(activity, getString(resId),
                    (dialog, which) -> {
                        LoginActivity.start(activity);
                        activity.finish();
                    }).show();
        }
    }
}
