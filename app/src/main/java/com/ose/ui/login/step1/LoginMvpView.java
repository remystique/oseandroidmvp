package com.ose.ui.login.step1;


import com.ose.ui.base.MvpView;

public interface LoginMvpView extends MvpView {
    void removeEmailError();

    void goRegister();

    void goLogIn();

    void setEmailError();

    void initView();

    void showOnBoarding();

    void goGame();
}
