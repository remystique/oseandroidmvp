package com.ose.ui.login.step2;


import com.ose.ui.base.MvpView;

public interface LoginStep2MvpView extends MvpView {

    void initView();

    void activateButtonLetsGo();

    void goGame();
}
