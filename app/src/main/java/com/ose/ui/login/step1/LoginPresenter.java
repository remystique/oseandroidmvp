package com.ose.ui.login.step1;

import com.facebook.stetho.common.StringUtil;
import com.google.firebase.iid.FirebaseInstanceId;
import com.ose.data.UserManager;
import com.ose.data.model.game.DailyQuestionsAnswer;
import com.ose.data.model.user.LoginAnswer;
import com.ose.ui.base.BasePresenter;
import com.ose.ui.common.AbstractSubscriber;
import com.ose.ui.game.GameMvpView;

import android.util.Log;

import java.net.HttpURLConnection;

import javax.inject.Inject;

import retrofit2.Response;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginPresenter extends BasePresenter<LoginMvpView> {

    private final UserManager mUserManager;

    private Subscription mSubscription;

    @Inject
    public LoginPresenter(UserManager userManager) {
        mUserManager = userManager;
    }

    @Override
    public void attachView(LoginMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void checkEmail(String email) {
        final LoginMvpView loginView = getMvpView();
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            loginView.removeEmailError();
            loginView.showLoader();
            mSubscription = mUserManager.checkEmail(email)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new AbstractSubscriber<Response<Void>, LoginMvpView>(null, getMvpView()) {

                        @Override
                        public void onNext(Response<Void> voidResponse) {
                            loginView.stopLoader();
                            switch (voidResponse.raw().code()) {
                                case HttpURLConnection.HTTP_NOT_FOUND:
                                    loginView.goRegister();
                                    break;
                                case HttpURLConnection.HTTP_NO_CONTENT:
                                    loginView.goLogIn();
                                    break;
                                default:
                                    loginView.showError();
                            }
                        }
                    });
        } else {
            loginView.setEmailError();
        }
    }

    public void initView() {
        getMvpView().initView();
        LoginAnswer userInformations = mUserManager.getUserInformations();
        if (userInformations != null) {
            mUserManager.registerDevice(FirebaseInstanceId.getInstance().getToken())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new AbstractSubscriber<Response<Void>, LoginMvpView>(null, getMvpView()) {});
            getMvpView().goGame();
        } else {
            if (mUserManager.getFirstTimeSetting()) {
                getMvpView().showOnBoarding();
            }
        }
    }
}
