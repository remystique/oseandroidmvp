package com.ose.ui.login.step2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.content.res.AppCompatResources;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.ose.R;
import com.ose.ui.base.BaseLoaderActivity;
import com.ose.ui.common.TopRoundedDrawable;
import com.ose.ui.home.HomeActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ose.utils.DeviceUtil.isAlwaysFinishActivitiesOptionEnabled;

public class LoginStep2Activity extends BaseLoaderActivity implements LoginStep2MvpView {

    private static final String EMAIL = "email";

    @Inject
    LoginStep2Presenter mLoginStep2Presenter;

    @BindView(R.id.email)
    TextInputLayout mEmail;

    @BindView(R.id.password)
    TextInputLayout mPassword;

    @BindView(R.id.lets_go)
    TextView mTextViewLetsGo;

    @BindView(R.id.header_image)
    ImageView mHeaderImage;

    @BindView(R.id.header_message)
    TextView mHeaderMessage;

    public static Intent intent(final Context context, String email) {
        final Intent intent = new Intent(context, LoginStep2Activity.class);
        intent.putExtra(EMAIL, email);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    public static void start(final Context context, View view, String email) {
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(((Activity) context), view, context.getString(R.string.login_form_email));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && !isAlwaysFinishActivitiesOptionEnabled(context)) {
            context.startActivity(intent(context, email), options.toBundle());
        } else {
            context.startActivity(intent(context, email));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_login_step2);
        ButterKnife.bind(this);

        mLoginStep2Presenter.attachView(this);
        mLoginStep2Presenter.initView();
    }

    @Override
    public void activateButtonLetsGo() {
        mTextViewLetsGo.setTextColor(Color.WHITE);
        mTextViewLetsGo.setBackground(AppCompatResources.getDrawable(this, R.drawable.red_round_btn));
        mTextViewLetsGo.setOnClickListener(view ->
                mLoginStep2Presenter.login(mEmail.getEditText().getText().toString(),
                mPassword.getEditText().getText().toString()));
    }

    @Override
    public void goGame() {
        HomeActivity.start(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLoginStep2Presenter.detachView();
    }

    @Override
    public void initView() {
        setupToolbar();
        setupHeader();
        mEmail.getEditText().setText(getIntent().getStringExtra(EMAIL));
        setupPasswordWatcher();
    }

    private void setupHeader() {
        final int startColor = ContextCompat.getColor(this, R.color.colorPrimaryDark);
        final int endColor = ContextCompat.getColor(this, R.color.colorPrimary);
        mHeaderImage.setImageDrawable(new TopRoundedDrawable(startColor, endColor, this));
        mHeaderMessage.setText(R.string.login_header_welcome);
    }

    private void setupPasswordWatcher() {
        mPassword.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mLoginStep2Presenter.activateButtonLetsGo();
            }
        });
    }

    private void setupActionDone() {
        mPassword.getEditText().setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mLoginStep2Presenter.login(mEmail.getEditText().getText().toString(), mPassword.getEditText().getText().toString());
                return true;
            }
            return false;
        });
    }

}
