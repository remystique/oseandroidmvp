package com.ose.ui.login.step2;

import com.google.firebase.iid.FirebaseInstanceId;
import com.ose.data.UserManager;
import com.ose.data.model.user.LoginAnswer;
import com.ose.data.model.user.post.LoginBody;
import com.ose.ui.base.BasePresenter;
import com.ose.ui.common.AbstractSubscriber;
import com.ose.ui.login.step1.LoginMvpView;

import android.util.Log;

import java.net.HttpURLConnection;

import javax.inject.Inject;

import retrofit2.Response;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginStep2Presenter extends BasePresenter<LoginStep2MvpView> {

    private final UserManager mUserManager;

    private Subscription mSubscription;

    @Inject
    public LoginStep2Presenter(UserManager userManager) {
        mUserManager = userManager;
    }

    @Override
    public void attachView(LoginStep2MvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void initView() {
        getMvpView().initView();
    }

    public void activateButtonLetsGo() {
        getMvpView().activateButtonLetsGo();
    }


    public void login(String email, String password) {
        final LoginStep2MvpView loginStep2MvpView = getMvpView();
        loginStep2MvpView.showLoader();
        mSubscription = mUserManager.login(
                LoginBody.builder().clientId("ose-app")
                        .clientSecret("a455c1deccffda4fac015a8e9327e46e")
                        .grantType("password")
                        .username(email)
                        .password(password)
                        .build())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnNext(loginAnswer -> {
                    mUserManager.saveUserInformations(loginAnswer);
                    mUserManager.registerDevice(FirebaseInstanceId.getInstance().getToken())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(new AbstractSubscriber<Response<Void>, LoginStep2MvpView>(null, getMvpView()) {});
                })
                .subscribe(new AbstractSubscriber<LoginAnswer, LoginStep2MvpView>(null, getMvpView()) {

                    @Override
                    public void onNext(LoginAnswer loginAnswer) {
                        loginStep2MvpView.stopLoader();
                        mUserManager.saveUserInformations(loginAnswer);
                        loginStep2MvpView.goGame();
                    }
                });
    }
}
