package com.ose.ui.login.step1;

import com.google.firebase.iid.FirebaseInstanceId;

import com.ose.R;
import com.ose.ui.base.BaseLoaderActivity;
import com.ose.ui.common.TopRoundedDrawable;
import com.ose.ui.home.HomeActivity;
import com.ose.ui.login.step2.LoginStep2Activity;
import com.ose.ui.onboarding.OnBoardingActivity;
import com.ose.ui.register.step1.RegisterStep1Activity;
import com.ose.ui.setting.SettingsActivity;
import com.ose.utils.ViewUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ose.utils.DeviceUtil.isAlwaysFinishActivitiesOptionEnabled;

public class LoginActivity extends BaseLoaderActivity implements LoginMvpView {

    @Inject
    LoginPresenter mLoginPresenter;

    @BindView(R.id.header_image)
    ImageView mHeaderImage;

    @BindView(R.id.header_message)
    TextView mHeaderMessage;

    @BindView(R.id.email)
    TextInputLayout mEmail;

    @BindView(R.id.next)
    ImageView mNextButton;

    @OnClick(R.id.next)
    public void onNextClick() {
        mLoginPresenter.checkEmail(mEmail.getEditText().getText().toString());
    }

    public static Intent intent(final Context context) {
        final Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    public static void start(final Context context) {
            context.startActivity(intent(context));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mLoginPresenter.attachView(this);
        mLoginPresenter.initView();
    }

    private void deleteIt() {
        mEmail.getEditText().setText("test@ose-app.com");
    }

    private void setupActionDone() {
        mEmail.getEditText().setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mLoginPresenter.checkEmail(mEmail.getEditText().getText().toString());
                return true;
            }
            return false;
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLoginPresenter.detachView();
    }

    @Override
    public void goRegister() {
        RegisterStep1Activity.start(this, mEmail, mEmail.getEditText().getText().toString());
    }

    @Override
    public void goLogIn() {
        LoginStep2Activity.start(this, mEmail, mEmail.getEditText().getText().toString());
    }

    @Override
    public void setEmailError() {
        ViewUtil.hideKeyboard(this);
        mEmail.setError(getString(R.string.bad_mail));
    }

    @Override
    public void initView() {
        setupToolbar();
        setupHeader();
        setupActionDone();
    }

    @Override
    public void showOnBoarding() {
        OnBoardingActivity.start(this);
    }

    @Override
    public void goGame() {
        HomeActivity.start(this);
    }

    @Override
    protected void setupToolbar() {
        super.setupToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    private void setupHeader() {
        final int startColor = ContextCompat.getColor(this, R.color.colorPrimaryDark);
        final int endColor = ContextCompat.getColor(this, R.color.colorPrimary);
        mHeaderImage.setImageDrawable(new TopRoundedDrawable(startColor, endColor, this));
        mHeaderMessage.setText(R.string.login_header_enter_mail);
    }


    @Override
    public void removeEmailError() {
        ViewUtil.hideKeyboard(this);
        mEmail.setError(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (com.ose.BuildConfig.DEBUG) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.setting_menu, menu);
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.option:
                SettingsActivity.start(this);
                return true;
        }
        return false;
    }
}
