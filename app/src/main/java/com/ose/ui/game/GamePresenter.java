package com.ose.ui.game;

import com.ose.data.ErrorManager;
import com.ose.data.GameManager;
import com.ose.data.model.game.DailyQuestionsAnswer;
import com.ose.data.model.game.post.AnswerQuestion;
import com.ose.data.model.game.post.AnswerQuestionBody;
import com.ose.ui.base.BasePresenter;
import com.ose.ui.common.AbstractSubscriber;

import android.util.Log;

import java.net.HttpURLConnection;
import java.util.List;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class GamePresenter extends BasePresenter<GameMvpView> {


    GameManager mGameManager;
    ErrorManager mErrorManager;

    private Subscription mSubscription;

    @Inject
    public GamePresenter(GameManager gameManager, ErrorManager errorManager) {
        mGameManager = gameManager;
        mErrorManager = errorManager;
    }

    @Override
    public void attachView(GameMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void initView(boolean showSnackBar) {
        final GameMvpView gameMvpView = getMvpView();
        gameMvpView.showLoader();
        mSubscription = mGameManager.getDailyQuestions()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new AbstractSubscriber<DailyQuestionsAnswer, GameMvpView>(mErrorManager, getMvpView()) {
                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable,  e -> {
                            Log.e("TAG", "" + e.toString());
                            if (e.getLocalizedMessage().contains("500")) {
                                gameMvpView.showProgressionFragment(showSnackBar);
                            } else {
                                gameMvpView.showError();
                            }
                            return null;
                        });
                    }

                    @Override
                    public void onNext(DailyQuestionsAnswer dailyQuestionsAnswer) {
                        gameMvpView.stopLoader();
                        if (dailyQuestionsAnswer.getData().size() > 0) {
                            gameMvpView.initView(dailyQuestionsAnswer);
                        } else {
                            gameMvpView.showProgressionFragment(showSnackBar);
                        }
                        saveCurrentQuestions();
                    }
                });
    }

    private void saveCurrentQuestions() {

    }

    public void answerQuestions(List<AnswerQuestion> answerQuestions) {
        final GameMvpView gameMvpView = getMvpView();
        gameMvpView.showLoader();
        mSubscription = mGameManager.answerQuestions(AnswerQuestionBody.builder().answers(answerQuestions).build())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new AbstractSubscriber<ResponseBody, GameMvpView>(mErrorManager, getMvpView()) {
                    @Override
                    public void onNext(ResponseBody responseBody) {
                        gameMvpView.showProgression();
                    }
                });
    }
}
