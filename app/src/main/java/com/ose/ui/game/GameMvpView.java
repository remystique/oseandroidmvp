package com.ose.ui.game;


import com.ose.data.model.game.DailyQuestionsAnswer;
import com.ose.ui.base.MvpView;

public interface GameMvpView extends MvpView {

    void initView(DailyQuestionsAnswer dailyQuestionsAnswer);

    void showProgression();

    void showProgressionFragment(boolean showSnackBar);
}
