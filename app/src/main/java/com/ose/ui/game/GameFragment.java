package com.ose.ui.game;

import com.ose.R;
import com.ose.data.model.game.DailyQuestionsAnswer;
import com.ose.data.model.game.Question;
import com.ose.data.model.game.post.AnswerQuestion;
import com.ose.ui.base.BaseLoaderFragment;
import com.ose.ui.common.widget.DayProgressionView;
import com.ose.ui.progression.ProgressionWeekFromDayActivity;
import com.ose.ui.progression.ProgressionWeekFromEndActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GameFragment extends BaseLoaderFragment implements GameMvpView {

    private static final int REQUEST_CODE = 1001;
    @Inject
    GamePresenter mGamePresenter;

    List<Question> mCurrentQuestions;
    List<AnswerQuestion> mCurrentAnswers;

    @BindView(R.id.day_progression_view)
    DayProgressionView mDayProgressionView;

    @BindView(R.id.answer_one)
    TextView mAnswerOne;

    @BindView(R.id.answer_two)
    TextView mAnswerTwo;

    private int mCurrentQuestionIndex;
    private Listener mListener;

    @OnClick(R.id.progression_view_layout)
    public void onProgressionLatyoutClick(View view) {
        ProgressionWeekFromDayActivity.start(getContext(), mCurrentQuestionIndex);
    }

    @OnClick({R.id.answer_one, R.id.answer_two})
    public void onAnswerClick(View view) {
        final int answerIndex;
        switch (view.getId()) {
            case R.id.answer_one:
                answerIndex = 0;
                break;
            default:
                answerIndex = 1;
        }
        fillAnswers(answerIndex);
        if (mCurrentQuestionIndex == 9) {
            mGamePresenter.answerQuestions(mCurrentAnswers);
        } else {
            nextQuestion();
        }
    }

    private void fillAnswers(int answerIndex) {
        final Question question = mCurrentQuestions.get(mCurrentQuestionIndex);
        mCurrentAnswers.add(AnswerQuestion.builder()
                .question(question.getSlug())
                .answer(question.getAnswers().get(answerIndex).getKey())
                .build());
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Listener) {
            mListener = (Listener) context;
        }
        getActivityComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_game, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGamePresenter.attachView(this);
        mGamePresenter.initView(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGamePresenter.detachView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mGamePresenter = null;
        mListener = null;
    }

    @Override
    public void initView(DailyQuestionsAnswer dailyQuestionsAnswer) {
        mCurrentAnswers = new ArrayList<>();
        mCurrentQuestions = dailyQuestionsAnswer.getData();
        mCurrentQuestionIndex = 0;
        initQuestion();
        initDayProgressView();
    }

    @Override
    public void showProgression() {
        ProgressionWeekFromEndActivity.startForResult(this, REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            mGamePresenter.initView(true);
        }
    }

    @Override
    public void showProgressionFragment(boolean showSnackBar) {
        mListener.onShowProgression(showSnackBar);
    }

    private void initDayProgressView() {
        mDayProgressionView.setupView(mCurrentQuestionIndex);
    }

    private void initQuestion() {
        Question question = mCurrentQuestions.get(mCurrentQuestionIndex);
        mAnswerOne.setText(question.getAnswers().get(0).getContent());
        mAnswerTwo.setText(question.getAnswers().get(1).getContent());
    }

    public void nextQuestion() {
        mCurrentQuestionIndex++;
        mDayProgressionView.setupView(mCurrentQuestionIndex);
        initQuestion();
    }

    @Override
    public void showError() {
        super.showError();
    }

    public interface Listener {
        void onShowProgression(boolean showSnackBar);
    }
}
