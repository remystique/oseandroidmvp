package com.ose.ui.progression;


import com.ose.ui.base.MvpView;

import java.util.List;

public interface ProgressionMvpView extends MvpView {

    void initView(List<Integer> progressions);
}
