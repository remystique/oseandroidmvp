package com.ose.ui.progression;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.ose.R;
import com.ose.ui.base.BaseLoaderActivity;
import com.ose.ui.common.widget.WeekProgressionView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProgressionWeekFromEndActivity extends BaseLoaderActivity implements ProgressionMvpView {

    @Inject
    ProgressionPresenter mProgressionPresenter;

    @BindView(R.id.week_progression_view)
    WeekProgressionView mWeekProgressionView;

    public static Intent intent(final Context context) {
        final Intent intent = new Intent(context, ProgressionWeekFromEndActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    public static void startForResult(Fragment fragment, int requestCode) {
        fragment.startActivityForResult(intent(fragment.getContext()), requestCode);
    }

    public static void start(final Context context) {
        context.startActivity(intent(context));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_progression_week_from_end);
        ButterKnife.bind(this);

        mProgressionPresenter.attachView(this);
        mProgressionPresenter.initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mProgressionPresenter.detachView();
    }

    @Override
    public void initView(List<Integer> progressions) {
        setupToolbar();
        mWeekProgressionView.setData(progressions);
    }
}
