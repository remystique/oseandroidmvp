package com.ose.ui.progression;

import android.util.Log;

import com.ose.data.ErrorManager;
import com.ose.data.GameManager;
import com.ose.data.model.game.ProgressionAnswer;
import com.ose.ui.base.BasePresenter;
import com.ose.ui.common.AbstractSubscriber;
import com.ose.ui.profile.ProfileMvpView;

import java.net.HttpURLConnection;

import javax.inject.Inject;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ProgressionPresenter extends BasePresenter<ProgressionMvpView> {

    private final GameManager mGameManager;
    ErrorManager mErrorManager;

    private Subscription mSubscription;

    @Inject
    public ProgressionPresenter(GameManager gameManager, ErrorManager errorManager) {
        mGameManager = gameManager;
        mErrorManager = errorManager;
    }

    @Override
    public void attachView(ProgressionMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void initView() {
        final ProgressionMvpView mvpView = getMvpView();
        mvpView.showLoader();
        mSubscription = mGameManager.getProgression()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new AbstractSubscriber<ProgressionAnswer, ProgressionMvpView>(mErrorManager, getMvpView()) {
                    @Override
                    public void onNext(ProgressionAnswer progressionAnswer) {
                        mvpView.stopLoader();
                        mvpView.initView(progressionAnswer.getData());
                    }
                });
    }
}
