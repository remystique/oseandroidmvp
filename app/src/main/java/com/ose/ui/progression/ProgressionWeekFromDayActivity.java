package com.ose.ui.progression;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.ose.R;
import com.ose.ui.base.BaseLoaderActivity;
import com.ose.ui.common.widget.DayProgressionView;
import com.ose.ui.common.widget.WeekProgressionView;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static org.joda.time.DateTimeConstants.*;

public class ProgressionWeekFromDayActivity extends BaseLoaderActivity implements ProgressionMvpView {

    private static final String PROGRESSION = "progression";

    @Inject
    ProgressionPresenter mProgressionPresenter;

    @BindView(R.id.week_progression_view)
    WeekProgressionView mWeekProgressionView;

    @BindView(R.id.day_progression_view)
    DayProgressionView mDayProgressionView;

    public static Intent intent(final Context context, int progression) {
        final Intent intent = new Intent(context, ProgressionWeekFromDayActivity.class);
        intent.putExtra(PROGRESSION, progression);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    public static void start(final Context context, int progression) {
        context.startActivity(intent(context, progression));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_progression_week_from_day);
        ButterKnife.bind(this);

        mProgressionPresenter.attachView(this);
        mProgressionPresenter.initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mProgressionPresenter.detachView();
    }

    @Override
    public void initView(List<Integer> progressions) {
        setupToolbar();
        int dayProgression = getIntent().getIntExtra(PROGRESSION, 0);

        if (dayProgression != 0) {
            final DateTime now = DateTime.now();
            final int progressionStart;
            switch (now.dayOfWeek().get()) {
                case MONDAY:
                    progressionStart = 0;
                    break;
                case TUESDAY:
                    progressionStart = 10;
                    break;
                case WEDNESDAY:
                    progressionStart = 20;
                    break;
                case THURSDAY:
                    progressionStart = 30;
                    break;
                case FRIDAY:
                    progressionStart = 40;
                    break;
                default:
                    progressionStart = 0;
                    break;
            }
            LinkedList<Integer> progressionWeekAndDay = new LinkedList<>();
            progressionWeekAndDay.addAll(progressions);
            for (int i = progressionStart; i <= progressionStart + dayProgression; i++) {
                progressionWeekAndDay.add(i, 0);
            }
            mWeekProgressionView.setData(progressionWeekAndDay);
        } else {
            mWeekProgressionView.setData(progressions);
        }

        mDayProgressionView.setupView(dayProgression);
    }
}
