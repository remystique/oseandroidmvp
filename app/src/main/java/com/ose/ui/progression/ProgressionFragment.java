package com.ose.ui.progression;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ose.R;
import com.ose.ui.base.BaseLoaderFragment;
import com.ose.ui.common.widget.WeekProgressionView;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProgressionFragment extends BaseLoaderFragment implements ProgressionMvpView {

    @Inject
    ProgressionPresenter mProgressionPresenter;

    @BindView(R.id.progression_view)
    WeekProgressionView mWeekProgressionView;

    @BindView(R.id.progression_sentence)
    TextView mProgressionSentence;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        getActivityComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_progression, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProgressionPresenter.attachView(this);
        mProgressionPresenter.initView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mProgressionPresenter.detachView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mProgressionPresenter = null;
    }

    @Override
    public void initView(List<Integer> progressions) {
        mWeekProgressionView.setData(progressions);
        setProgressionSentence();
    }

    private void setProgressionSentence() {
        Calendar myDate = Calendar.getInstance();
        switch (myDate.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.SUNDAY :
            case Calendar.SATURDAY :
                mProgressionSentence.setText(R.string.end_weekend);
                break;
            default:
                mProgressionSentence.setText(R.string.endgame_phrase_tomorrow);
        }
    }
}
