package com.ose.ui.onboarding;


import com.ose.ui.base.MvpView;

public interface OnBoardingMvpView extends MvpView {

    void goLogIn();

    void initView();
}
