package com.ose.ui.onboarding;

import com.ose.R;
import com.ose.ui.base.BaseActivity;
import com.ose.ui.home.PagerAdapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

public class OnBoardingActivity extends BaseActivity implements OnBoardingMvpView {

    @Inject
    OnBoardingPresenter mOnBoardingPresenter;

    @BindView(R.id.on_boarding_pager)
    ViewPager mViewPager;

    @BindView(R.id.indicator)
    CircleIndicator mCircleIndicator;

    @BindView(R.id.start_game)
    TextView mStartGameButton;

    @BindView(R.id.button)
    View mButton;

    @BindView(R.id.on_boarding_text)
    TextView mOnBoardingText;

    private int[] mBackgrounds = {
            R.drawable.ob_screen_1,
            R.drawable.ob_screen_2,
            R.drawable.ob_screen_4,
            R.drawable.ob_screen_5,
            R.drawable.ob_screen_6,
            R.drawable.ob_screen_7,
            R.drawable.ob_screen_9
    };

    private int[] mTexts = {
            R.string.OB_one,
            R.string.OB_two,
            R.string.OB_three,
            R.string.OB_four,
            R.string.OB_five,
            R.string.OB_six,
            R.string.OB_seven,
            R.string.OB_eight,
    };

    private Point[] mButtonPositions = {
            new Point(183, 158),
            new Point(166, 295),
            new Point(0, 0),
            new Point(0, 0),
            new Point(188, 273),
            new Point(217, 113),
            new Point(217, 167),
    };

    private int mPagerPosition = 0;

    public static Intent intent(final Context context) {
        final Intent intent = new Intent(context, OnBoardingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    public static void start(final Context context) {
        context.startActivity(intent(context));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_on_boarding);
        ButterKnife.bind(this);

        mOnBoardingPresenter.attachView(this);
        mOnBoardingPresenter.initView();

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mPagerPosition = position;
                mOnBoardingText.post(() -> setText(position));
                mButton.post(() -> setButtonPosition(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mViewPager.post(() -> {
            setButtonPosition(mPagerPosition);
            setText(mPagerPosition);
        });

    }

    private void setText(int position) {
        mOnBoardingText.setText(mTexts[position]);
    }

    private void setButtonPosition(int position) {
        final Point buttonPosition = mButtonPositions[position];
        if (buttonPosition.x == 0) {
            mButton.setVisibility(View.GONE);
        } else {
            mButton.setVisibility(View.VISIBLE);
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.WRAP_CONTENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(getXPosition(buttonPosition.x), getYPosition(buttonPosition.y), 0, 0);
            mButton.setLayoutParams(params);
        }
    }

    private int getYPosition(int y) {
        return (y * mViewPager.getMeasuredHeight() / 622) - (mButton.getHeight() / 4);
    }

    private int getXPosition(int x) {
        return (x * mViewPager.getMeasuredWidth() / 375) - (mButton.getWidth() / 4);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mOnBoardingPresenter.detachView();
    }

    @Override
    public void goLogIn() {
        finish();
    }

    @Override
    public void initView() {
        if (getSupportFragmentManager().getFragments() != null) {
            getSupportFragmentManager().getFragments().clear();
        }

        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        for (int i = 0; i < mBackgrounds.length; i++) {
            adapter.addFrag(OnBoardingFragment.newInstance(mBackgrounds[i], mTexts[i]));
        }
        mViewPager.setAdapter(adapter);
        mCircleIndicator.setViewPager(mViewPager);
        adapter.registerDataSetObserver(mCircleIndicator.getDataSetObserver());

        mButton.setOnClickListener(v -> {
            if (mPagerPosition == adapter.getCount() - 1) {
                finish();
                mOnBoardingPresenter.userSeenOnBoarding();
            }
            mViewPager.setCurrentItem(mPagerPosition + 1);
        });

        mStartGameButton.setOnClickListener(v -> {
            finish();
            mOnBoardingPresenter.userSeenOnBoarding();
        });
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void stopLoader() {

    }

    @Override
    public void showError() {

    }

    @Override
    public void showError(int resId) {

    }

    @Override
    public void disconnect() {

    }

    @Override
    public void disconnect(int resId) {

    }
}
