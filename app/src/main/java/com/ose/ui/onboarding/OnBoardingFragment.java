package com.ose.ui.onboarding;

import com.ose.R;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OnBoardingFragment extends Fragment {

    public static final String BACKGROUND_RES = "BACKGROUND_RES";

    public static final String TEXT_RES = "TEXT_RES";

    @BindView(R.id.background)
    ImageView mBackground;

    public static Fragment newInstance(int backgroundRes, int textRes) {
        OnBoardingFragment myFragment = new OnBoardingFragment();

        Bundle args = new Bundle();
        args.putInt(BACKGROUND_RES, backgroundRes);
        args.putInt(TEXT_RES, textRes);
        myFragment.setArguments(args);

        return myFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_on_boarding, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mBackground.setBackgroundResource(getArguments().getInt(BACKGROUND_RES));
        mBackground.setBackgroundResource(getArguments().getInt(BACKGROUND_RES));
    }
}
