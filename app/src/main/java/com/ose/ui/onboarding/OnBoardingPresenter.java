package com.ose.ui.onboarding;

import com.ose.data.OnBoardingManager;
import com.ose.ui.base.BasePresenter;

import javax.inject.Inject;

public class OnBoardingPresenter extends BasePresenter<OnBoardingMvpView> {

    private final OnBoardingManager mOnBoardingManager;

    @Inject
    public OnBoardingPresenter(OnBoardingManager onBoardingManager) {
        mOnBoardingManager = onBoardingManager;
    }

    @Override
    public void attachView(OnBoardingMvpView mvpView) {
        super.attachView(mvpView);
    }

    public void initView() {
        getMvpView().initView();
    }

    public void userSeenOnBoarding() {
        mOnBoardingManager.saveFirstTimeSetting(false);
    }
}
