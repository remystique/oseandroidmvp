package com.ose.ui.home;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ose.ui.game.GameFragment;
import com.ose.ui.profile.menu.ProfileMenuFragment;
import com.ose.ui.progression.ProgressionFragment;
import com.ose.ui.thread.ThreadsFragment;

public class HomePagerAdapter extends FragmentStatePagerAdapter {

    public static final int FRAGMENT_NUMBER = 3;
    private boolean isGame;

    public HomePagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                if (isGame) {
                    return new GameFragment();
                } else {
                    return new ProgressionFragment();
                }
            case 1:
                    return new ThreadsFragment();
            case 2:
                return new ProfileMenuFragment();
            default:
                throw new IllegalStateException("not supported");
        }
    }

    @Override
    public int getCount() {
        return FRAGMENT_NUMBER;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public void setGame(boolean game) {
        isGame = game;
    }

}