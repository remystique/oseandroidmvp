package com.ose.ui.home;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.widget.Switch;

import com.ose.ui.game.GameFragment;
import com.ose.ui.profile.menu.ProfileMenuFragment;
import com.ose.ui.progression.ProgressionFragment;
import com.ose.ui.thread.ThreadsFragment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();

    public PagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFrag(Fragment fragment) {
        mFragmentList.add(fragment);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    public void replaceFragment(int position, Fragment fragment) {
        mFragmentList.remove(position);
        mFragmentList.add(position, fragment);
        notifyDataSetChanged();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}