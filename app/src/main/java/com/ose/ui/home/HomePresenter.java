package com.ose.ui.home;

import com.ose.data.GameManager;
import com.ose.ui.base.BasePresenter;
import com.ose.ui.game.GameFragment;

import javax.inject.Inject;

import rx.Subscription;

public class HomePresenter extends BasePresenter<HomeMvpView> {


    GameManager mGameManager;

    private Subscription mSubscription;

    @Inject
    public HomePresenter(GameManager gameManager) {
        mGameManager = gameManager;
    }

    @Override
    public void attachView(HomeMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void initView() {
        getMvpView().initView(true);
    }
}
