package com.ose.ui.home;


import android.support.v4.app.Fragment;

import com.ose.ui.base.MvpView;

public interface HomeMvpView extends MvpView {
    void initView(boolean isGame);
}
