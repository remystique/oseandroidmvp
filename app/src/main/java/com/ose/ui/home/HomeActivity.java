package com.ose.ui.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.ose.R;
import com.ose.ui.base.BaseActivity;
import com.ose.ui.game.GameFragment;
import com.ose.ui.profile.menu.ProfileMenuFragment;
import com.ose.ui.progression.ProgressionFragment;
import com.ose.ui.thread.ThreadDiscussionActivity;
import com.ose.ui.thread.ThreadsFragment;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity implements HomeMvpView, GameFragment.Listener {

    public static final String NOTIFICATION_THREAD_ID = "NOTIFICATION_THREAD_ID";
    @Inject
    HomePresenter homePresenter;

    @BindView(R.id.navigation)
    BottomNavigationView bottomNavigationView;

    public static Intent intent(final Context context) {
        final Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return intent;
    }

    public static void start(final Context context) {
        context.startActivity(intent(context));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        homePresenter.attachView(this);
        homePresenter.initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        homePresenter.detachView();
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void stopLoader() {

    }

    @Override
    public void showError() {

    }

    @Override
    public void showError(int resId) {

    }

    @Override
    public void disconnect() {

    }

    @Override
    public void disconnect(int resId) {

    }

    @Override
    public void initView(boolean isGame) {
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            Fragment selectedFragment = null;
            switch (item.getItemId()) {
                case R.id.game_menu:
                    selectedFragment = isGame ? new GameFragment() : new ProgressionFragment();
                    break;
                case R.id.thread_menu:
                    selectedFragment = new ThreadsFragment();
                    break;
                case R.id.profile_menu:
                    selectedFragment = new ProfileMenuFragment();
                    break;
            }
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content, selectedFragment);
            transaction.commit();
            return true;
        });

        String threadId = getIntent().getStringExtra(NOTIFICATION_THREAD_ID);
        if (threadId != null && !"".equals(threadId)) {
            bottomNavigationView.getMenu().getItem(1).setChecked(true);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content, new ThreadsFragment());
            transaction.commit();
            ThreadDiscussionActivity.start(this, threadId);
            getIntent().putExtra(NOTIFICATION_THREAD_ID, "");
        } else {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content, isGame ? new GameFragment() : new ProgressionFragment());
            transaction.commit();
        }
    }

    @Override
    public void onShowProgression(boolean showSnackBar) {
        initView(false);
        if (showSnackBar && itIsFriday()) {
            showSnackBar();
        }
    }

    private boolean itIsFriday() {
        Calendar myDate = Calendar.getInstance();
        return myDate.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY;
    }

    private void showSnackBar() {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.content), R.string.endgame_phrase_friday, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.ok, v -> snackbar.dismiss());
        snackbar.show();
    }
}
