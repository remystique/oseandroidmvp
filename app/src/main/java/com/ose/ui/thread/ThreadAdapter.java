package com.ose.ui.thread;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.ose.R;
import com.ose.data.model.interaction.Message;
import com.ose.data.model.interaction.Relation;
import com.ose.data.model.interaction.Thread;
import com.ose.data.model.interaction.view.ThreadKind;
import com.ose.data.model.interaction.view.ThreadView;
import com.ose.data.model.stringdef.RelationStatus;
import com.ose.utils.DialogFactory;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ThreadAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String EMPTY_STRING = "";
    private final Context mContext;
    private final ArrayList<ThreadView> mThreadViews;

    private Listener mListener;

    public ThreadAdapter(Context context, List<ThreadView> threadViews) {
        super();
        mContext = context;
        mThreadViews = new ArrayList<>();
        mThreadViews.addAll(threadViews);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, @ThreadKind int viewType) {
        final View v;
        switch (viewType) {
            case ThreadKind.MATCHED:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.thread_matched_item, viewGroup, false);
                return new ThreadMatchedViewHolder(v);
            case ThreadKind.TITLE:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.thread_title_item, viewGroup, false);
                return new ThreadTitleViewHolder(v);
            default:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.thread_unknown_item, viewGroup, false);
                return new ThreadUnknownViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        @ThreadKind final int itemViewType = getItemViewType(position);
        final ThreadView threadView = mThreadViews.get(position);
        switch (itemViewType) {
            case ThreadKind.MATCHED:
                setupMatchedView((ThreadMatchedViewHolder) holder, position);
                break;
            case ThreadKind.TITLE:
                setupTitleView((ThreadTitleViewHolder) holder, threadView);
                break;
            case ThreadKind.UNMATCHED:
                setupUnmatchedView((ThreadUnknownViewHolder) holder, threadView);
                break;
            case ThreadKind.UNKNOWN:
                setupUnknownView((ThreadUnknownViewHolder) holder, position);
                break;
        }

    }

    private void setupMatchedView(final ThreadMatchedViewHolder holder, final int position) {
        final ThreadView threadView = mThreadViews.get(position);
        final Thread thread = threadView.getThread();
        final String picture = thread.getOther().getPicture();
        if (!TextUtils.isEmpty(picture)) {
            Glide.with(mContext).load(picture)
                    .asBitmap().centerCrop().into(new BitmapImageViewTarget(holder.mPicture) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    holder.mPicture.setImageDrawable(circularBitmapDrawable);
                }
            });
        }
        holder.mFirstName.setText(thread.getOther().getFirstName());
        String lastMessage = getLastMessage(thread);
        holder.mLastMessage.setText(lastMessage);
        if (!EMPTY_STRING.equals(lastMessage)) {
            holder.mLastMessageDate.setText(thread.getLastMessage().getDate().withZone(DateTimeZone.getDefault()).toString("HH:mm"));
        }
        setListener(holder, threadView);
        holder.mLastMessageDate.setVisibility(View.VISIBLE);
        holder.mUnmatchButton.setVisibility(View.GONE);
        holder.mUnmatchButton.setOnClickListener(null);
        holder.itemView.setOnLongClickListener(v -> {
            holder.mLastMessageDate.setVisibility(View.GONE);
            holder.mUnmatchButton.setVisibility(View.VISIBLE);
            holder.mUnmatchButton.setOnClickListener(view ->
                    DialogFactory.createGenericYesNoDialog(holder.mUnmatchButton.getContext(), holder.mUnmatchButton.getContext().getString(R.string.are_you_sure),
                            (dialog, which) -> {
                                unmatchThread(ThreadKind.MATCHED, position);
                                holder.mUnmatchButton.postDelayed(this::notifyDataSetChanged, 300);
                            }).show());
            return true;
        });
    }

    private void setupTitleView(ThreadTitleViewHolder holder, ThreadView threadView) {
        holder.mTitle.setText(threadView.getTitle());
    }

    private void setupUnknownView(ThreadUnknownViewHolder holder, int position) {
        final ThreadView threadView = mThreadViews.get(position);
        final Thread thread = threadView.getThread();
        holder.mFirstName.setText(thread.getOther().getFirstName());
        holder.mLastMessage.setText(getLastMessage(thread));
        setListener(holder, threadView);
        if (RelationStatus.MATCHED.equals(threadView.getThread().getMe().getStatus())) {
            holder.mMatchButton.setImageResource(R.drawable.ic_hourglass);
            holder.mMatchButton.setOnClickListener(null);
        } else {
            holder.mMatchButton.setImageResource(R.drawable.yellow_tick);
            holder.mMatchButton.setOnClickListener(view -> {
                if (RelationStatus.MATCHED.equals(threadView.getThread().getOther().getStatus())) {
                    matchThread(position);
                } else {
                    final ThreadView newThreadView = threadView.toBuilder()
                            .thread(thread.toBuilder()
                                    .me(thread.getMe().toBuilder().status(RelationStatus.MATCHED).build())
                                    .build())
                            .build();
                    mThreadViews.remove(position);
                    mThreadViews.add(position, newThreadView);
                    mListener.onMatchThread(newThreadView);
                }
                holder.mMatchButton.postDelayed(this::notifyDataSetChanged, 300);
            });
        }

        holder.mUnmatchButton.setOnClickListener(view ->
                DialogFactory.createGenericYesNoDialog(holder.mUnmatchButton.getContext(), holder.mUnmatchButton.getContext().getString(R.string.are_you_sure),
                        (dialog, which) -> {
                            unmatchThread(ThreadKind.UNKNOWN, position);
                            holder.mUnmatchButton.postDelayed(this::notifyDataSetChanged, 300);
                        }).show());
    }

    private void setupUnmatchedView(ThreadUnknownViewHolder holder, ThreadView threadView) {
        final Thread thread = threadView.getThread();
        holder.itemView.setBackgroundResource(R.color.missed_relation_background);
        holder.mFirstName.setText(thread.getOther().getFirstName());
        holder.mLastMessage.setText(getLastMessage(thread));
        holder.mMatchedButtonsLayout.setVisibility(View.GONE);
    }

    private String getLastMessage(Thread thread) {
        Message lastMessage = thread.getLastMessage();
        return lastMessage != null ? lastMessage.getContent() : EMPTY_STRING;
    }


    private void setListener(RecyclerView.ViewHolder holder, final ThreadView threadView) {
        holder.itemView.setOnClickListener(v -> mListener.onThreadSelected(threadView.getThread()));
    }

    @Override
    @ThreadKind
    public int getItemViewType(int position) {
        return mThreadViews.get(position).getKind();
    }

    @Override
    public int getItemCount() {
        return mThreadViews.size();
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public void unmatchThread(@ThreadKind int previousKind, int position) {
        final ThreadView threadView = mThreadViews.get(position);
        final ThreadView newThreadView = threadView.toBuilder().kind(ThreadKind.UNMATCHED).build();
        if (hasUnmatchedThreads()) {
            final int toPosition = findUnmatchedIndex(position);
            mThreadViews.remove(position);
            mThreadViews.add(toPosition, newThreadView);
            notifyItemMoved(position, toPosition);
        } else {
            mThreadViews.remove(position);
            mThreadViews.addAll(mListener.onUnmatchThread(newThreadView));
            notifyDataSetChanged();
        }
        if (previousKind == ThreadKind.UNKNOWN) {
            if (!hasUnknownThreads()) {
                mThreadViews.remove(position - 1);
                notifyDataSetChanged();
            }
        } else {
            if (!hasMatchedThreads()) {
                mThreadViews.remove(position - 1);
                notifyDataSetChanged();
            }
        }
    }

    private boolean hasUnmatchedThreads() {
        for (ThreadView threadView : mThreadViews) {
            if (threadView.getKind() == ThreadKind.UNMATCHED) {
                return true;
            }
        }
        return false;
    }

    private boolean hasUnknownThreads() {
        for (ThreadView threadView : mThreadViews) {
            if (threadView.getKind() == ThreadKind.UNKNOWN) {
                return true;
            }
        }
        return false;
    }

    public void matchThread(int position) {
        final ThreadView threadView = mThreadViews.get(position);
        final ThreadView newThreadView = threadView.toBuilder().kind(ThreadKind.MATCHED).build();
        if (hasMatchedThreads()) {
            final int toPosition = findMatchedIndex(position);
            mThreadViews.remove(position);
            mThreadViews.add(toPosition, newThreadView);
            notifyItemMoved(position, toPosition);
        } else {
            final int toPosition = findNewMatchIndexPosition(position);
            mThreadViews.remove(position);
            mThreadViews.addAll(toPosition, mListener.onMatchThread(newThreadView));
            notifyDataSetChanged();
        }
        if (!hasUnknownThreads()) {
            mThreadViews.remove(position - 1);
            notifyDataSetChanged();
        }
    }

    private int findNewMatchIndexPosition(int position) {
        for (int i = position; i < mThreadViews.size() - 1; i++) {
            if (getItemViewType(i) != ThreadKind.UNKNOWN) {
                return i - 1;
            }
        }
        return mThreadViews.size() - 1;
    }

    private boolean hasMatchedThreads() {
        for (ThreadView threadView : mThreadViews) {
            if (threadView.getKind() == ThreadKind.MATCHED) {
                return true;
            }
        }
        return false;
    }

    private int findMatchedIndex(int position) {
        for (int i = position; i < mThreadViews.size() - 1; i++) {
            if (getItemViewType(i) == ThreadKind.MATCHED) {
                return i - 1;
            }
        }
        return mThreadViews.size() - 1;
    }

    private int findUnmatchedIndex(int position) {
        for (int i = position; i < mThreadViews.size() - 1; i++) {
            if (getItemViewType(i) == ThreadKind.UNMATCHED) {
                return i - 1;
            }
        }
        return mThreadViews.size() - 1;
    }

    public interface Listener {
        void onThreadSelected(Thread thread);

        List<ThreadView> onMatchThread(ThreadView threadView);

        List<ThreadView> onUnmatchThread(ThreadView threadView);
    }

    class ThreadTitleViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView mTitle;


        public ThreadTitleViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    class ThreadMatchedViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.picture)
        ImageView mPicture;
        @BindView(R.id.first_name)
        TextView mFirstName;
        @BindView(R.id.last_message)
        TextView mLastMessage;
        @BindView(R.id.last_message_date)
        TextView mLastMessageDate;
        @BindView(R.id.unmatch_button)
        ImageView mUnmatchButton;


        public ThreadMatchedViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    class ThreadUnknownViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.first_name)
        TextView mFirstName;
        @BindView(R.id.last_message)
        TextView mLastMessage;
        @BindView(R.id.matched_buttons_layout)
        View mMatchedButtonsLayout;
        @BindView(R.id.match_button)
        ImageView mMatchButton;
        @BindView(R.id.unmatch_button)
        ImageView mUnmatchButton;


        public ThreadUnknownViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}

