package com.ose.ui.thread;


import com.ose.data.model.interaction.ThreadProfile;
import com.ose.data.model.interaction.view.MessageView;
import com.ose.ui.base.MvpView;

import java.util.List;

public interface ThreadDiscussionView extends MvpView {
    void initView(List<MessageView> threadDiscussionAnswer);

    void initHeader(String firstName, String picture);

    void initProfile(ThreadProfile threadProfile);

    void showMessageLoader();

    void stopMessageLoader();

    void addMessage(MessageView messageView);

    void addMessageView(List<MessageView> newMessageViews);

    void onUserMatched();

    void onUserUnmatched();

    void showProfileLoader();

    void stopProfileLoader();
}
