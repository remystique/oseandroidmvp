package com.ose.ui.thread;

import com.ose.R;
import com.ose.data.model.interaction.Thread;
import com.ose.data.model.interaction.ThreadsAnswer;
import com.ose.data.model.interaction.view.ThreadKind;
import com.ose.data.model.interaction.view.ThreadView;
import com.ose.data.model.stringdef.RelationStatus;
import com.ose.ui.base.BaseLoaderFragment;
import com.ose.utils.DialogFactory;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ThreadsFragment extends BaseLoaderFragment implements ThreadsMvpView {

    @Inject
    ThreadsPresenter mThreadsPresenter;

    @BindView(R.id.threads)
    RecyclerView mThreadsRecyclerView;

    @BindView(R.id.place_holder)
    View mPlaceHolder;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        getActivityComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_threads, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mThreadsPresenter.attachView(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mThreadsPresenter.initView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mThreadsPresenter.detachView();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mThreadsPresenter = null;
    }

    @Override
    public void initView(ThreadsAnswer threadsAnswer) {
        ThreadAdapter threadAdapter = new ThreadAdapter(getContext(), toThreadViews(threadsAnswer));
        threadAdapter.setListener(new ThreadAdapter.Listener() {
            @Override
            public void onThreadSelected(Thread thread) {
                ThreadDiscussionActivity.start(getContext(), thread.getId());
            }

            @Override
            public List<ThreadView> onMatchThread(ThreadView threadView) {
                mThreadsPresenter.matchThread(threadView.getThread());
                List<ThreadView> threadViews = new ArrayList<>();
                threadViews.add(ThreadView.builder().kind(ThreadKind.TITLE).title(getString(R.string.chat_relation_phrase)).build());
                threadViews.add(threadView);
                return threadViews;
            }

            @Override
            public List<ThreadView> onUnmatchThread(ThreadView threadView) {
                mThreadsPresenter.unmatchThread(threadView.getThread());
                List<ThreadView> threadViews = new ArrayList<>();
                threadViews.add(ThreadView.builder().kind(ThreadKind.TITLE).title(getString(R.string.chat_missed_phrase)).build());
                threadViews.add(threadView);
                return threadViews;
            }
        });

        mThreadsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mThreadsRecyclerView.setAdapter(threadAdapter);
        if (threadAdapter.getItemCount() > 0) {
            mPlaceHolder.setVisibility(View.GONE);
        }
    }

    private List<ThreadView> toThreadViews(ThreadsAnswer threadsAnswer) {
        List<ThreadView> threadViews = new ArrayList<>();
        final List<Thread> threads = threadsAnswer.getData();
        final ArrayList<Thread> threadsMatched = new ArrayList<>();
        final ArrayList<Thread> threadsUnknown = new ArrayList<>();
        final ArrayList<Thread> threadsUnmatched = new ArrayList<>();

        for (Thread thread : threads) {
            @RelationStatus final String meStatus = thread.getMe().getStatus();
            @RelationStatus String otherStatus = thread.getOther().getStatus();
            if (RelationStatus.MATCHED.equals(meStatus)) {
                switch (otherStatus) {
                    case RelationStatus.MATCHED:
                        threadsMatched.add(thread);
                        break;
                    case RelationStatus.UNKNOWN:
                        threadsUnknown.add(thread);
                        break;
                    case RelationStatus.UNMATCHED:
                        threadsUnmatched.add(thread);
                        break;
                }
            } else if (RelationStatus.UNKNOWN.equals(meStatus)) {
                switch (otherStatus) {
                    case RelationStatus.MATCHED:
                    case RelationStatus.UNKNOWN:
                        threadsUnknown.add(thread);
                        break;
                    case RelationStatus.UNMATCHED:
                        threadsUnmatched.add(thread);
                        break;
                }
            } else if (RelationStatus.UNMATCHED.equals(meStatus)) {
                threadsUnmatched.add(thread);
            }
        }

        if (!threadsUnknown.isEmpty()) {
            threadViews.add(ThreadView.builder().kind(ThreadKind.TITLE).title(getString(R.string.chat_meet_phrase)).build());
            for (Thread thread : threadsUnknown) {
                threadViews.add(ThreadView.builder().kind(ThreadKind.UNKNOWN).thread(thread).build());
            }
        }

        if (!threadsMatched.isEmpty()) {
            threadViews.add(ThreadView.builder().kind(ThreadKind.TITLE).title(getString(R.string.chat_relation_phrase)).build());
            for (Thread thread : threadsMatched) {
                threadViews.add(ThreadView.builder().kind(ThreadKind.MATCHED).thread(thread).build());
            }
        }

        if (!threadsUnmatched.isEmpty()) {
            threadViews.add(ThreadView.builder().kind(ThreadKind.TITLE).title(getString(R.string.chat_missed_phrase)).build());
            for (Thread thread : threadsUnmatched) {
                threadViews.add(ThreadView.builder().kind(ThreadKind.UNMATCHED).thread(thread).build());
            }
        }

        return threadViews;
    }

    @Override
    public void showError() {
        DialogFactory.createGenericErrorDialog(getContext(), getString(R.string.error_message)).show();
        //mThreadsPresenter.initView();
    }
}
