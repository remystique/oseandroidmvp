package com.ose.ui.thread;


import com.ose.data.model.interaction.ThreadsAnswer;
import com.ose.ui.base.MvpView;

public interface ThreadsMvpView extends MvpView {

    void initView(ThreadsAnswer threadsAnswer);
}
