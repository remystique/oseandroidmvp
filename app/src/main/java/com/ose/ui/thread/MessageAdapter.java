package com.ose.ui.thread;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ose.R;
import com.ose.data.model.interaction.Message;
import com.ose.data.model.interaction.view.MessageKind;
import com.ose.data.model.interaction.view.MessageView;

import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private final ArrayList<MessageView> mMessageViews;

    public MessageAdapter(Context context, List<MessageView> messageViews) {
        super();
        mContext = context;
        mMessageViews = new ArrayList<>();
        mMessageViews.addAll(messageViews);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, @MessageKind int viewType) {
        final View v;
        switch (viewType) {
            case MessageKind.TITLE:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.message_title_item, viewGroup, false);
                return new MessageTitleViewHolder(v);
            case MessageKind.OTHER_MESSAGE:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.message_other_item, viewGroup, false);
                return new MessageOtherViewHolder(v);
            default:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.message_owner_item, viewGroup, false);
                return new MessageOwnerViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        @MessageKind final int itemViewType = getItemViewType(position);
        final MessageView messageView = mMessageViews.get(position);
        switch (itemViewType) {
            case MessageKind.TITLE:
                setupTitleMessageView((MessageTitleViewHolder) holder, messageView);
                break;
            case MessageKind.OTHER_MESSAGE:
                setupOtherMessageView((MessageOtherViewHolder) holder, messageView);
                break;
            case MessageKind.OWNER_MESSAGE:
                setupOwnerMessageView((MessageOwnerViewHolder) holder, messageView);
                break;
        }

    }

    private void setupTitleMessageView(MessageTitleViewHolder holder, MessageView messageView) {
        holder.mTitle.setText(messageView.getTitle());
    }

    private void setupOtherMessageView(final MessageOtherViewHolder holder, MessageView messageView) {
        final Message message = messageView.getMessage();
        holder.mMessage.setText(message.getContent());
        holder.mMessageDate.setText(message.getDate().withZone(DateTimeZone.getDefault()).toString("HH:mm"));
    }


    private void setupOwnerMessageView(MessageOwnerViewHolder holder, MessageView messageView) {
        final Message message = messageView.getMessage();
        holder.mMessage.setText(message.getContent());
        holder.mMessageDate.setText(message.getDate().withZone(DateTimeZone.getDefault()).toString("HH:mm"));
    }

    @Override
    @MessageKind
    public int getItemViewType(int position) {
        return mMessageViews.get(position).getKind();
    }

    @Override
    public int getItemCount() {
        return mMessageViews.size();
    }

    public List<MessageView> getMessageViews() {
        return mMessageViews;
    }

    public void addMessages(List<MessageView> newMessageViews) {
        getMessageViews().addAll(newMessageViews);
        notifyItemInserted(getItemCount() - 1 + newMessageViews.size());
    }

    public interface Listener {
    }

    class MessageTitleViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView mTitle;

        public MessageTitleViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    class MessageOwnerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.message)
        TextView mMessage;
        @BindView(R.id.message_date)
        TextView mMessageDate;


        public MessageOwnerViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    class MessageOtherViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.message)
        TextView mMessage;
        @BindView(R.id.message_date)
        TextView mMessageDate;


        public MessageOtherViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}

