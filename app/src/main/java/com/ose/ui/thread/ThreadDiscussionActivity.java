package com.ose.ui.thread;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ose.R;
import com.ose.data.model.interaction.ThreadProfile;
import com.ose.data.model.interaction.view.MessageView;
import com.ose.data.model.stringdef.RelationStatus;
import com.ose.ui.base.BaseLoaderActivity;
import com.ose.ui.login.step1.LoginActivity;
import com.ose.utils.DialogFactory;
import com.ose.utils.KeyboardUtils;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ThreadDiscussionActivity extends BaseLoaderActivity implements ThreadDiscussionView {

    public static final String THREAD_ID = "THREAD_ID";

    @Inject
    ThreadDiscussionPresenter mThreadDiscussionPresenter;

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.thread_picture)
    ImageView mThreadPicture;

    @BindView(R.id.send_message)
    ImageView mSendMessage;

    @BindView(R.id.send_message_loader)
    View mSendMessageLoader;

    @BindView(R.id.first_name)
    TextView mFirstName;

    @BindView(R.id.birthday)
    TextView mBirthday;

    @BindView(R.id.locality)
    TextView mLocality;

    @BindView(R.id.message)
    TextView mMessage;

    @BindView(R.id.messages)
    RecyclerView mThreadsRecyclerView;

    @BindView(R.id.match_button)
    View mMatchButton;
    @BindView(R.id.unmatch_button)
    View mUnmatchButton;
    @BindView(R.id.report_button)
    View mReportButton;

    @BindView(R.id.profile_loader)
    View mProfileLoader;

    private MessageAdapter mMessageAdapter;
    private String mPicture;

    public static Intent intent(final Context context, String threadId) {
        final Intent intent = new Intent(context, ThreadDiscussionActivity.class);
        intent.putExtra(THREAD_ID, threadId);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    public static void start(final Context context, String threadId) {
        context.startActivity(intent(context, threadId));
    }

    @OnClick(R.id.send_message)
    public void onSendMessage() {
        final String message = mMessage.getText().toString();
        if (!TextUtils.isEmpty(message)) {
            mThreadDiscussionPresenter.sendMessage(getIntent().getStringExtra(THREAD_ID), message);
            mSendMessage.setEnabled(false);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_thread_discussion);
        ButterKnife.bind(this);
        setupToolbar();
        showStatusBar();
        setStatusBarColor(R.color.yellow_status_dark);

        mThreadDiscussionPresenter.attachView(this);
        mThreadDiscussionPresenter.initView(getIntent().getStringExtra(THREAD_ID));
    }

    private void setKeyboardListener() {
        KeyboardUtils.addKeyboardToggleListener(this, isVisible -> {
            if (isVisible) {
                mThreadsRecyclerView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
            }
        });
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String threadId = getIntent().getStringExtra(THREAD_ID);
        mThreadDiscussionPresenter.saveTchatThreadId(threadId);
        mThreadDiscussionPresenter.initView(threadId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mThreadDiscussionPresenter.detachView();
    }

    @Override
    public void initView(List<MessageView> messageViews) {
        mMessageAdapter = new MessageAdapter(this, messageViews);
        mThreadsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mThreadsRecyclerView.setAdapter(mMessageAdapter);
        mThreadsRecyclerView.scrollToPosition(mMessageAdapter.getItemCount() - 1);

        setKeyboardListener();
    }

    @Override
    public void initHeader(String firstName, String picture) {
        setupToolbarTitle(firstName);
        setupPictures(picture);
    }

    @Override
    public void initProfile(ThreadProfile threadProfile) {
        mFirstName.setText(threadProfile.getFirstName());
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        LocalDate birthDate = null ;
        if (threadProfile.getBirthDate() != null) {
            birthDate = formatter.parseLocalDate(threadProfile.getBirthDate());
        }
        if (birthDate != null) {
            final Years threadAge = Years.yearsBetween(birthDate, LocalDate.now());
            mBirthday.setText(String.format("%s %s", threadAge.getYears(), getString(R.string.match_age_text)));
        } else {
            mBirthday.setText("");
        }
        mLocality.setText(threadProfile.getCity());

        mReportButton.setOnClickListener(v -> {
            Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
            sendIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            sendIntent.setData(Uri.parse("mailto:contact@ose-app.com"));
            sendIntent.putExtra(Intent.EXTRA_TEXT, getThreadInformations(threadProfile));
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.relation_report));
            startActivity(sendIntent);
        });

        if (!RelationStatus.MATCHED.equals(threadProfile.getStatus())) {
            mMatchButton.setVisibility(View.VISIBLE);
            mMatchButton.setOnClickListener(v -> mThreadDiscussionPresenter.matchThread(threadProfile.getUserId()));
        }

        mUnmatchButton.setOnClickListener(
                v -> DialogFactory.createGenericYesNoDialog(this, getString(R.string.are_you_sure),
                (dialog, which) -> mThreadDiscussionPresenter.unmatchThread(threadProfile.getUserId()))
                .show());
    }

    private String getThreadInformations(ThreadProfile threadProfile) {
        return String.format("%s\n%s\n%s\n%s\n\n",
                getString(R.string.report_thread_id, getIntent().getStringExtra(THREAD_ID)),
                getString(R.string.report),
                threadProfile.getUserId(),
                getString(R.string.report_because));
    }

    @Override
    public void showMessageLoader() {
        mSendMessageLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopMessageLoader() {
        mSendMessageLoader.setVisibility(View.GONE);
    }

    @Override
    public void addMessage(MessageView messageView) {
        mThreadDiscussionPresenter.addMessage(mMessageAdapter.getMessageViews(), messageView);
    }

    @Override
    public void addMessageView(List<MessageView> newMessageViews) {
        mMessageAdapter.addMessages(newMessageViews);
        mThreadsRecyclerView.scrollToPosition(mMessageAdapter.getItemCount() - 1);
        mMessage.setText("");
        mSendMessage.setEnabled(true);
    }

    @Override
    public void onUserMatched() {
        mMatchButton.setVisibility(View.GONE);
    }

    @Override
    public void onUserUnmatched() {
        onBackPressed();
    }

    @Override
    public void showProfileLoader() {
        mProfileLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopProfileLoader() {
        mProfileLoader.setVisibility(View.GONE);
    }


    @Override
    public void showError() {
        super.showError();
        mSendMessage.setEnabled(true);
    }

    @Override
    protected void setupToolbarTitle(String title) {
        super.setupToolbarTitle(title);
    }

    private void setupPictures(String picture) {
        mPicture = picture;
        setPicture(picture, mThreadPicture);
    }

    private void setPicture(String picture, final ImageView pictureView) {
        Glide.with(this).load(picture).asBitmap().centerCrop().into(new BitmapImageViewTarget(pictureView) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                pictureView.setImageDrawable(circularBitmapDrawable);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.thread_discussion_menu, menu);
        final MenuItem item = menu.getItem(0);
        Glide.with(this).load(mPicture).asBitmap().centerCrop()
                .into(new SimpleTarget<Bitmap>(150, 150) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        item.setIcon(circularBitmapDrawable);
                    }
                });
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.thread_profile:
                mDrawerLayout.openDrawer(GravityCompat.END);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
