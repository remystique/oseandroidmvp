package com.ose.ui.thread;

import android.util.Log;

import com.ose.data.ErrorManager;
import com.ose.data.InteractionManager;
import com.ose.data.model.interaction.Thread;
import com.ose.data.model.interaction.ThreadProfileAnswer;
import com.ose.data.model.interaction.ThreadsAnswer;
import com.ose.ui.base.BasePresenter;
import com.ose.ui.common.AbstractSubscriber;

import java.net.HttpURLConnection;

import javax.inject.Inject;

import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ThreadsPresenter extends BasePresenter<ThreadsMvpView> {


    InteractionManager mInteractionManager;
    ErrorManager mErrorManager;

    private Subscription mSubscription;

    @Inject
    public ThreadsPresenter(InteractionManager interactionManager, ErrorManager errorManager) {
        mInteractionManager = interactionManager;
        mErrorManager = errorManager;
    }

    @Override
    public void attachView(ThreadsMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void initView() {
        final ThreadsMvpView mvpView = getMvpView();
        mvpView.showLoader();
        mSubscription = mInteractionManager.getThreads()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new AbstractSubscriber<ThreadsAnswer, ThreadsMvpView>(mErrorManager, getMvpView()) {
                    @Override
                    public void onNext(ThreadsAnswer threadsAnswer) {
                        mvpView.stopLoader();
                        mvpView.initView(threadsAnswer);
                    }
                });
    }

    public void matchThread(Thread thread) {
        mSubscription = mInteractionManager.matchUser(thread.getOther().getId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new AbstractSubscriber<String, ThreadsMvpView>(mErrorManager, getMvpView()) {});
    }

    public void unmatchThread(Thread thread) {
        mSubscription = mInteractionManager.unmatchUser(thread.getOther().getId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new AbstractSubscriber<String, ThreadsMvpView>(mErrorManager, getMvpView()) {});
    }
}
