package com.ose.ui.thread;

import android.util.Log;

import com.ose.data.ErrorManager;
import com.ose.data.InteractionManager;
import com.ose.data.model.game.ProgressionAnswer;
import com.ose.data.model.interaction.Message;
import com.ose.data.model.interaction.Thread;
import com.ose.data.model.interaction.ThreadDiscussion;
import com.ose.data.model.interaction.ThreadDiscussionAnswer;
import com.ose.data.model.interaction.ThreadProfileAnswer;
import com.ose.data.model.interaction.view.MessageKind;
import com.ose.data.model.interaction.view.MessageView;
import com.ose.ui.base.BasePresenter;
import com.ose.ui.common.AbstractSubscriber;
import com.ose.ui.progression.ProgressionMvpView;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ThreadDiscussionPresenter extends BasePresenter<ThreadDiscussionView> {


    InteractionManager mInteractionManager;
    ErrorManager mErrorManager;

    private Subscription mSubscription;

    @Inject
    public ThreadDiscussionPresenter(InteractionManager interactionManager, ErrorManager errorManager) {
        mInteractionManager = interactionManager;
        mErrorManager = errorManager;
    }

    @Override
    public void attachView(ThreadDiscussionView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void initView(String threadId) {
        final ThreadDiscussionView mvpView = getMvpView();
        mvpView.showLoader();
        mvpView.showProfileLoader();
        mSubscription = mInteractionManager.getThreadDiscussion(threadId)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(threadDiscussionAnswer -> {
                    mvpView.initHeader(threadDiscussionAnswer.getData().getFirstName(), threadDiscussionAnswer.getData().getPicture());
                    mvpView.initView(toMessageViews(threadDiscussionAnswer));
                    mvpView.stopLoader();
                })
                .observeOn(Schedulers.io())
                .map(threadDiscussionAnswer -> mInteractionManager.getThreadProfile(threadDiscussionAnswer.getData().getUserId()))
                .map(threadProfileAnswerObservable -> threadProfileAnswerObservable.toBlocking().first())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new AbstractSubscriber<ThreadProfileAnswer, ThreadDiscussionView>(mErrorManager, getMvpView()) {
                    @Override
                    public void onNext(ThreadProfileAnswer threadProfileAnswer) {
                        mvpView.stopProfileLoader();
                        mvpView.initProfile(threadProfileAnswer.getData());
                    }
                });
    }

    private List<MessageView> toMessageViews(ThreadDiscussionAnswer threadDiscussionAnswer) {
        List<MessageView> messageViews = new ArrayList<>();
        final ThreadDiscussion body = threadDiscussionAnswer.getData();
        DateTime previousDate = null;
        for (Message message : body.getMessages()) {
            if (previousDate == null || Days.daysBetween(previousDate.toLocalDate(), message.getDate().toLocalDate()).getDays() > 0) {
                messageViews.add(MessageView.builder().kind(MessageKind.TITLE).title(message.getDate().toString("dd/MM/yyyy")).build());
                previousDate = message.getDate();
            }
            messageViews.add(MessageView.builder()
                    .kind(message.isOwner() ? MessageKind.OWNER_MESSAGE : MessageKind.OTHER_MESSAGE)
                    .message(message)
                    .build());
        }
        return messageViews;
    }

    public void sendMessage(String threadId, String message) {
        final ThreadDiscussionView mvpView = getMvpView();
        mvpView.showMessageLoader();
        mSubscription = mInteractionManager.sendMessage(threadId, message)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new AbstractSubscriber<Response<Void>, ThreadDiscussionView>(mErrorManager, getMvpView()) {
                    @Override
                    public void onNext(Response<Void> response) {
                        if (response.isSuccessful()) {
                            mvpView.stopMessageLoader();
                            mvpView.addMessage(
                                    MessageView.builder()
                                            .kind(MessageKind.OWNER_MESSAGE)
                                            .message(Message.builder()
                                                    .content(message)
                                                    .date(DateTime.now())
                                                    .isOwner(true)
                                                    .build())
                                            .build());
                        } else {
                            onError(new Exception(response.message()));
                        }

                    }
                });
    }

    public void addMessage(List<MessageView> messageViews, MessageView messageView) {
        List<MessageView> newMessageViews = new ArrayList<>();
        final Message message = messageView.getMessage();
        if (!messageViews.isEmpty()) {
            if (Days.daysBetween(
                    messageViews.get(messageViews.size() - 1).getMessage().getDate().toLocalDate()
                    , message.getDate().toLocalDate()).getDays() > 0) {
                newMessageViews.add(MessageView.builder().kind(MessageKind.TITLE).title(message.getDate().toString("dd/MM/yyyy")).build());
            }
        } else {
            newMessageViews.add(MessageView.builder().kind(MessageKind.TITLE).title(message.getDate().toString("dd/MM/yyyy")).build());
        }
        newMessageViews.add(messageView);
        getMvpView().addMessageView(newMessageViews);
    }

    public void matchThread(String threadId) {
        final ThreadDiscussionView mvpView = getMvpView();
        mvpView.showProfileLoader();
        mSubscription = mInteractionManager.matchUser(threadId)
                .flatMap(answer -> mInteractionManager.getThreadProfile(threadId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new AbstractSubscriber<ThreadProfileAnswer, ThreadDiscussionView>(mErrorManager, getMvpView()) {
                    @Override
                    public void onNext(ThreadProfileAnswer threadProfileAnswer) {
                        mvpView.stopProfileLoader();
                        mvpView.initProfile(threadProfileAnswer.getData());
                        mvpView.onUserMatched();
                    }
                });
    }

    public void unmatchThread(String threadId) {
        final ThreadDiscussionView mvpView = getMvpView();
        mvpView.showProfileLoader();
        mSubscription = mInteractionManager.unmatchUser(threadId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new AbstractSubscriber<String, ThreadDiscussionView>(mErrorManager, getMvpView()) {
                    @Override
                    public void onNext(String s) {
                        mvpView.onUserUnmatched();
                    }
                });
    }

    public void saveTchatThreadId(String threadId) {
        mInteractionManager.saveTchatThreadId(threadId);
    }
}
