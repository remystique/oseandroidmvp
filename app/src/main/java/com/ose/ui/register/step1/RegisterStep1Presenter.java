package com.ose.ui.register.step1;

import com.ose.data.UserManager;
import com.ose.ui.base.BasePresenter;

import javax.inject.Inject;

import rx.Subscription;

public class RegisterStep1Presenter extends BasePresenter<RegisterStep1MvpView> {

    private final UserManager mUserManager;

    private Subscription mSubscription;

    @Inject
    public RegisterStep1Presenter(UserManager userManager) {
        mUserManager = userManager;
    }

    @Override
    public void attachView(RegisterStep1MvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void initView() {
        getMvpView().initView();
    }
}
