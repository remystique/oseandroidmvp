package com.ose.ui.register.step1;

import com.ose.R;
import com.ose.data.UserManager;
import com.ose.ui.base.BaseActivity;
import com.ose.ui.common.TopRoundedDrawable;
import com.ose.ui.common.widget.UserProfileView;
import com.ose.ui.register.step2.RegisterStep2Activity;
import com.ose.utils.DialogFactory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.ose.utils.DeviceUtil.isAlwaysFinishActivitiesOptionEnabled;

public class RegisterStep1Activity extends BaseActivity implements RegisterStep1MvpView {

    public static final int RATIO = 6;
    private static final String EXTRA_TRIGGER_SYNC_FLAG =
            "uk.co.ribot.androidboilerplate.ui.main.MainActivity.EXTRA_TRIGGER_SYNC_FLAG";
    private static final String EMAIL = "email";
    @Inject
    RegisterStep1Presenter mRegisterStep1Presenter;

    @Inject
    UserManager mUserManager;

    @BindView(R.id.header_message)
    TextView mHeaderText;

    @BindView(R.id.header_image)
    ImageView mHeaderImage;

    @BindView(R.id.user_profile_view)
    UserProfileView mUserProfileView;

    public static Intent intent(final Context context, String email) {
        final Intent intent = new Intent(context, RegisterStep1Activity.class);
        intent.putExtra(EMAIL, email);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    public static void start(final Context context, View view, String email) {
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(((Activity) context), view, context.getString(R.string.login_form_email));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && !isAlwaysFinishActivitiesOptionEnabled(context)) {
            context.startActivity(intent(context, email), options.toBundle());
        } else {
            context.startActivity(intent(context, email));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_register_step1);
        ButterKnife.bind(this);


        requestPermissionLocation();
        mRegisterStep1Presenter.attachView(this);
        mRegisterStep1Presenter.initView();

    }

    @Override
    protected void permissionGranted() {
        mUserProfileView.setupView(false, false);
    }

    @Override
    protected void permissionRefused() {
        mUserProfileView.setupView(false, true);
    }

    private void setupTitle() {
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRegisterStep1Presenter.detachView();
    }

    @Override
    public void showLoader() {
        //start loader
    }

    @Override
    public void stopLoader() {
        //stop loader
    }

    @Override
    public void showError() {
        DialogFactory.createGenericErrorDialog(this, "erreur").show();
    }

    @Override
    public void showError(int resId) {
        DialogFactory.createGenericErrorDialog(this, "erreur").show();
    }

    @Override
    public void disconnect() {

    }

    @Override
    public void disconnect(int resId) {

    }

    @Override
    public void initView() {
        setupToolbar();
        setupHeader();
        setupTitle();
        setupUserProfileViewListener();
    }

    private void setupUserProfileViewListener() {
        mUserProfileView.setListener(registerBody -> RegisterStep2Activity.start(RegisterStep1Activity.this, registerBody.toBuilder().email(getIntent().getStringExtra(EMAIL)).build()));
    }

    private void setupHeader() {
        final int startColor = ContextCompat.getColor(this, R.color.yellow_sign_in);
        final int endColor = ContextCompat.getColor(this, R.color.colorAccent);
        mHeaderImage.setImageDrawable(new TopRoundedDrawable(startColor, endColor, this));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mUserProfileView.onActivityResult(requestCode, resultCode, data);
    }
}
