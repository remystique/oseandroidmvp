package com.ose.ui.register.step2;

import com.google.firebase.iid.FirebaseInstanceId;
import com.ose.data.UserManager;
import com.ose.data.model.game.ProgressionAnswer;
import com.ose.data.model.user.LoginAnswer;
import com.ose.data.model.user.post.LoginBody;
import com.ose.data.model.user.post.RegisterBody;
import com.ose.ui.base.BasePresenter;
import com.ose.ui.common.AbstractSubscriber;
import com.ose.ui.login.step1.LoginMvpView;
import com.ose.ui.progression.ProgressionMvpView;

import android.util.Log;

import java.io.File;

import javax.inject.Inject;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RegisterStep2Presenter extends BasePresenter<RegisterStep2MvpView> {

    private final UserManager mUserManager;

    private Subscription mSubscription;

    @Inject
    public RegisterStep2Presenter(UserManager userManager) {
        mUserManager = userManager;
    }

    @Override
    public void attachView(RegisterStep2MvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void setupPicture(File imageFile) {
        getMvpView().setupPicture(imageFile);
    }

    public void initView() {
        getMvpView().initView();
    }

    public void register(final RegisterBody registerBody) {
        final RegisterStep2MvpView registerStep2MvpView = getMvpView();
        registerStep2MvpView.showLoader();

        mSubscription = mUserManager.register(registerBody)
                .map(registerAnswer -> mUserManager.login(
                        LoginBody.builder().clientId("ose-app")
                                .clientSecret("a455c1deccffda4fac015a8e9327e46e")
                                .grantType("password")
                                .username(registerBody.getEmail())
                                .password(registerAnswer.getPassword())
                                .build()))
                .map(loginAnswerObservable -> loginAnswerObservable.toBlocking().first())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnNext(loginAnswer -> {
                    mUserManager.saveUserInformations(loginAnswer);
                    mUserManager.registerDevice(FirebaseInstanceId.getInstance().getToken())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(new AbstractSubscriber<Response<Void>, RegisterStep2MvpView>(null, getMvpView()) {});
                })
                .subscribe(new AbstractSubscriber<LoginAnswer, RegisterStep2MvpView>(null, getMvpView()) {
                    @Override
                    public void onNext(LoginAnswer loginAnswer) {
                        registerStep2MvpView.stopLoader();
                        mUserManager.saveUserInformations(loginAnswer);
                        registerStep2MvpView.goGame();
                    }
                });
    }
}
