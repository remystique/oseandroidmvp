package com.ose.ui.register.step1;


import com.ose.ui.base.MvpView;

public interface RegisterStep1MvpView extends MvpView {

    void initView();
}
