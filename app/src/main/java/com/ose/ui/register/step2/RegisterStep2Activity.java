package com.ose.ui.register.step2;

import com.ose.R;
import com.ose.data.UserManager;
import com.ose.data.model.user.post.RegisterBody;
import com.ose.ui.base.BaseLoaderActivity;
import com.ose.ui.common.TopRoundedDrawable;
import com.ose.ui.common.widget.TakePictureView;
import com.ose.ui.home.HomeActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.content.res.AppCompatResources;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class RegisterStep2Activity extends BaseLoaderActivity implements RegisterStep2MvpView {

    private static final String EXTRA_TRIGGER_SYNC_FLAG =
            "uk.co.ribot.androidboilerplate.ui.main.MainActivity.EXTRA_TRIGGER_SYNC_FLAG";
    private static final String REGISTER_BODY = "registerBody";

    @Inject
    RegisterStep2Presenter mRegisterStep2Presenter;

    @Inject
    UserManager mUserManager;

    @BindView(R.id.lets_go)
    TextView mTextViewLetsGo;

    @BindView(R.id.profile_picture)
    TakePictureView mProfilePicture;

    @BindView(R.id.header_image)
    ImageView mHeaderImage;

    @BindView(R.id.header_message)
    TextView mHeaderMessage;

    public static Intent intent(final Context context, RegisterBody registerBody) {
        final Intent intent = new Intent(context, RegisterStep2Activity.class);
        intent.putExtra(REGISTER_BODY, registerBody);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    public static void start(final Context context, RegisterBody registerBody) {
        context.startActivity(intent(context, registerBody));
    }

    @OnClick(R.id.profile_picture)
    void onGetPictureClick(View view) {
        EasyImage.openChooserWithGallery(this, "choose picture", 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_register_step2);
        ButterKnife.bind(this);

        mRegisterStep2Presenter.attachView(this);
        mRegisterStep2Presenter.initView();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Toast.makeText(RegisterStep2Activity.this, "error", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                mRegisterStep2Presenter.setupPicture(imageFile);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRegisterStep2Presenter.detachView();
    }

    @Override
    public void setupPicture(File imageFile) {
        mProfilePicture.setImage(imageFile);
        setupLetsGoButton();
    }

    private void setupLetsGoButton() {
        mTextViewLetsGo.setTextColor(Color.WHITE);
        mTextViewLetsGo.setBackground(AppCompatResources.getDrawable(this, R.drawable.yellow_round_btn));
        mTextViewLetsGo.setOnClickListener(view -> {
            final RegisterBody registerBody = (RegisterBody) getIntent().getSerializableExtra(REGISTER_BODY);
            mRegisterStep2Presenter.register(registerBody.toBuilder().picture(mProfilePicture.getBase64Image()).build());
        });
    }

    @Override
    public void initView() {
        setupToolbar();
        setupHeader();
    }

    @Override
    public void goGame() {
        HomeActivity.start(this);
    }

    private void setupHeader() {
        final int startColor = ContextCompat.getColor(this, R.color.yellow_sign_in);
        final int endColor = ContextCompat.getColor(this, R.color.colorAccent);
        mHeaderImage.setImageDrawable(new TopRoundedDrawable(startColor, endColor, this));
        mHeaderMessage.setText(R.string.register_header_welcome);
    }


}
