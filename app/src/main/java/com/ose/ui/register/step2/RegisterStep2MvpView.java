package com.ose.ui.register.step2;


import com.ose.ui.base.MvpView;

import java.io.File;

public interface RegisterStep2MvpView extends MvpView {

    void setupPicture(File imageFile);

    void initView();

    void goGame();
}
