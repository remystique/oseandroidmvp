package com.ose.ui.setting;

import com.ose.data.SettingsManager;
import com.ose.ui.base.BasePresenter;

import javax.inject.Inject;

import rx.Subscription;

public class SettingsPresenter extends BasePresenter<SettingsMvpView> {


    SettingsManager mSettingsManager;

    private Subscription mSubscription;

    @Inject
    public SettingsPresenter(SettingsManager settingsManager) {
        mSettingsManager = settingsManager;
    }

    @Override
    public void attachView(SettingsMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    public void setupView() {
        getMvpView().setMockSetting(mSettingsManager.getMockSetting());
        getMvpView().setMockDelaySetting(mSettingsManager.getMockDelaySetting());
    }

    public void setMockSetting(boolean isMocked) {
        mSettingsManager.saveMockSetting(isMocked);
    }

    public void setMockDelaySetting(int delaySetting) {
        mSettingsManager.saveMockDelaySetting(delaySetting);
    }
}
