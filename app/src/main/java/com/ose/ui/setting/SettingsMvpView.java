package com.ose.ui.setting;


import com.ose.ui.base.MvpView;

public interface SettingsMvpView extends MvpView {
    void setMockSetting(boolean isMocked);

    void setMockDelaySetting(Integer delaySetting);
}
