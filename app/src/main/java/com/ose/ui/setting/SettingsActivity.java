package com.ose.ui.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.ose.R;
import com.ose.ui.base.BaseActivity;
import com.ose.utils.DialogFactory;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsActivity extends BaseActivity implements SettingsMvpView {

    @Inject
    SettingsPresenter settingsPresenter;

    @BindView(R.id.mock_activator)
    SwitchCompat mockActivator;

    @BindView(R.id.mock_delay)
    EditText mockDelay;

    public static Intent intent(final Context context) {
        final Intent intent = new Intent(context, SettingsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    public static void start(final Context context) {
        context.startActivity(intent(context));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

        settingsPresenter.attachView(this);
        settingsPresenter.setupView();

        setMockSwitchListener();
    }

    private void setMockSwitchListener() {
        mockActivator.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                settingsPresenter.setMockSetting(isChecked);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        settingsPresenter.detachView();
    }

    @Override
    protected void onPause() {
        super.onPause();
        settingsPresenter.setMockDelaySetting(Integer.parseInt(mockDelay.getText().toString()));
    }

    @Override
    public void showLoader() {
//        mLoader.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopLoader() {
//        mLoader.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        DialogFactory.createGenericErrorDialog(this, getString(R.string.error_message)).show();
    }

    @Override
    public void showError(int resId) {
        DialogFactory.createGenericErrorDialog(this, getString(R.string.error_message)).show();
    }

    @Override
    public void disconnect() {

    }

    @Override
    public void disconnect(int resId) {

    }

    @Override
    public void setMockSetting(boolean isMocked) {
        mockActivator.setChecked(isMocked);
    }

    @Override
    public void setMockDelaySetting(Integer delaySetting) {
        mockDelay.setText(delaySetting.toString());
    }


}
