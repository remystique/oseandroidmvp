package com.ose.utils;

import android.content.Context;
import android.content.res.Resources;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import static android.os.Build.VERSION;
import static android.os.Build.VERSION_CODES;

/**
 * Created by remy_barbosa on 28/10/2016.
 */

public class DeviceUtil {

    public static final int ALWAYS_FINISH_ACTIVITIES_ENABLE = 1;

    public static boolean isAlwaysFinishActivitiesOptionEnabled(Context context) {
        int alwaysFinishActivitiesInt;
        if (VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN_MR1) {
            alwaysFinishActivitiesInt = Settings.System.getInt(context.getContentResolver(), Settings.Global.ALWAYS_FINISH_ACTIVITIES, 0);
        } else {
            alwaysFinishActivitiesInt = Settings.System.getInt(context.getContentResolver(), Settings.System.ALWAYS_FINISH_ACTIVITIES, 0);
        }
        return alwaysFinishActivitiesInt == ALWAYS_FINISH_ACTIVITIES_ENABLE;
    }

    public static int convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / 160f);
        return (int) px;
    }

    public static int getScreenWidth(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static int getScreenHeight(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

}
