package com.ose.injection.component;

import com.ose.injection.PerService;
import com.ose.injection.module.ServiceModule;
import com.ose.service.gcm.OseFirebaseInstanceIDService;
import com.ose.service.gcm.OseFirebaseMessagingService;

import dagger.Component;

/**
 * This component inject dependencies to all Services across the application
 */
@PerService
@Component(dependencies = ApplicationComponent.class, modules = ServiceModule.class)
public interface ServiceComponent {
    void inject(OseFirebaseInstanceIDService oseFirebaseInstanceIDService);

    void inject(OseFirebaseMessagingService oseFirebaseMessagingService);
}
