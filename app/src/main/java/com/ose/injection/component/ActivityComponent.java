package com.ose.injection.component;

import com.ose.injection.PerActivity;
import com.ose.injection.module.ActivityModule;
import com.ose.ui.base.BaseActivity;
import com.ose.ui.game.GameFragment;
import com.ose.ui.home.HomeActivity;
import com.ose.ui.login.step1.LoginActivity;
import com.ose.ui.login.step2.LoginStep2Activity;
import com.ose.ui.onboarding.OnBoardingActivity;
import com.ose.ui.profile.ProfileActivity;
import com.ose.ui.profile.menu.ProfileMenuFragment;
import com.ose.ui.progression.ProgressionFragment;
import com.ose.ui.progression.ProgressionWeekFromDayActivity;
import com.ose.ui.progression.ProgressionWeekFromEndActivity;
import com.ose.ui.register.step1.RegisterStep1Activity;
import com.ose.ui.register.step2.RegisterStep2Activity;
import com.ose.ui.setting.SettingsActivity;
import com.ose.ui.thread.ThreadDiscussionActivity;
import com.ose.ui.thread.ThreadsFragment;

import dagger.Component;

/**
 * This component inject dependencies to all Activities across the application
 */
@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {
    void inject(BaseActivity baseActivity);

    void inject(LoginActivity loginActivity);

    void inject(RegisterStep2Activity registerStep2Activity);

    void inject(RegisterStep1Activity registerStep1Activity);

    void inject(LoginStep2Activity loginStep2Activity);

    void inject(SettingsActivity settingsActivity);

    void inject(GameFragment gameFragment);

    void inject(HomeActivity homeActivity);

    void inject(ThreadsFragment threadsFragment);

    void inject(ThreadDiscussionActivity threadDiscussionActivity);

    void inject(ProfileMenuFragment profileMenuFragment);

    void inject(ProfileActivity profileActivity);

    void inject(ProgressionFragment progressionFragment);

    void inject(ProgressionWeekFromDayActivity progressionWeekFromDayActivity);

    void inject(ProgressionWeekFromEndActivity progressionWeekFromEndActivity);

    void inject(OnBoardingActivity onBoardingActivity);

}
