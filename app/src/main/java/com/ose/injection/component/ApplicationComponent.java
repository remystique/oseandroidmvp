package com.ose.injection.component;

import com.ose.data.BaseActivityManager;
import com.ose.data.ErrorManager;
import com.ose.data.GameManager;
import com.ose.data.InteractionManager;
import com.ose.data.OnBoardingManager;
import com.ose.data.SettingsManager;
import com.ose.data.UserManager;
import com.ose.injection.ApplicationContext;
import com.ose.injection.module.ApplicationModule;
import com.ose.injection.module.ServiceModule;
import com.ose.service.gcm.OseFirebaseInstanceIDService;
import com.ose.ui.base.BaseActivity;
import com.squareup.otto.Bus;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationModule.class
})

public interface ApplicationComponent {

    @ApplicationContext
    Context context();

    Application application();

    UserManager userManager();

    GameManager gameManager();

    ErrorManager errorManager();

    BaseActivityManager baseActivityManager();

    InteractionManager interactionManager();

    SettingsManager settingsManager();

    OnBoardingManager onBoardingManager();

    Bus eventBus();
}
