package com.ose.injection.module;

import com.ose.injection.ApplicationContext;

import android.app.Service;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class ServiceModule {

    private Service mService;

    public ServiceModule(Service service) {
        mService = service;
    }

    @Provides
    Service provideService() {
        return mService;
    }
}
