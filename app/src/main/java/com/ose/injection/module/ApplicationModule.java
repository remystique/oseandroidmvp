package com.ose.injection.module;

import com.ose.data.local.ConnexionPreferencesHelper;
import com.ose.data.remote.Creator;
import com.ose.data.remote.CreatorConnected;
import com.ose.data.remote.GameService;
import com.ose.data.remote.InteractionService;
import com.ose.data.remote.UserConnectedService;
import com.ose.data.remote.UserService;
import com.ose.injection.ApplicationContext;
import com.squareup.otto.Bus;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Provide application-level dependencies.
 */
@Module
public class ApplicationModule {
    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    Bus provideEventBus() {
        return new Bus();
    }

    @Provides
    @Singleton
    UserService provideUserService() {
        return (UserService) Creator.newService(UserService.class);
    }

    @Provides
    @Singleton
    UserConnectedService provideUserConnectedService(ConnexionPreferencesHelper connecionPreferenceHelper) {
        return (UserConnectedService) CreatorConnected.newService(UserConnectedService.class, connecionPreferenceHelper);
    }

    @Provides
    @Singleton
    GameService provideGameService(ConnexionPreferencesHelper connecionPreferenceHelper) {
        return (GameService) CreatorConnected.newService(GameService.class, connecionPreferenceHelper);
    }

    @Provides
    @Singleton
    InteractionService provideInteractionService(ConnexionPreferencesHelper connecionPreferenceHelper) {
        return (InteractionService) CreatorConnected.newService(InteractionService.class, connecionPreferenceHelper);
    }
}
