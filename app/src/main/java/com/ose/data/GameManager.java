package com.ose.data;

import com.ose.data.local.ConnexionPreferencesHelper;
import com.ose.data.model.game.DailyQuestionsAnswer;
import com.ose.data.model.game.ProgressionAnswer;
import com.ose.data.model.game.post.AnswerQuestionBody;
import com.ose.data.remote.GameService;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.ResponseBody;
import rx.Observable;

@Singleton
public class GameManager {

    private final GameService mGameService;

    private final ConnexionPreferencesHelper mConnexionPreferencesHelper;

    @Inject
    public GameManager(GameService gameService, ConnexionPreferencesHelper connexionPreferencesHelper) {
        mGameService = gameService;
        mConnexionPreferencesHelper = connexionPreferencesHelper;
    }

    public Observable<DailyQuestionsAnswer> getDailyQuestions() {
        return mGameService.getDailyQuestions();
    }

    public Observable<ResponseBody> answerQuestions(AnswerQuestionBody answerQuestions) {
        return mGameService.answerQuestions( answerQuestions);
    }

    public Observable<ProgressionAnswer> getProgression() {
        return mGameService.getProgression();
    }
}
