package com.ose.data.local;

import com.ose.injection.ApplicationContext;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class SettingsPreferencesHelper extends BasePreferencesHelper {

    private static final String MOCK = "mock";

    private static final String MOCK_DELAY = "MOCK_DELAY";

    private static final String FIRST_TIME = "FIRST_TIME";

    @Inject
    public SettingsPreferencesHelper(@ApplicationContext Context context) {
        super(context);
    }

    public void saveMockSetting(boolean isMocked) {
        saveBoolean(MOCK, isMocked);
    }

    public boolean getMockSetting() {
        return getBoolean(MOCK, false);
    }

    public void saveMockDelaySetting(int mockDelay) {
        saveInteger(MOCK_DELAY, mockDelay);
    }

    public int getMockDelaySetting() {
        return getInteger(MOCK_DELAY);
    }

    public void saveFirstTimeSetting(boolean isFirstTime) {
        saveBoolean(FIRST_TIME, isFirstTime);
    }

    public boolean getFirstTimeSetting() {
        return getBoolean(FIRST_TIME);
    }
}
