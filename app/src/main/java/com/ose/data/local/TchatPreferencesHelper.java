package com.ose.data.local;

import android.content.Context;

import com.ose.injection.ApplicationContext;

import javax.inject.Inject;
import javax.inject.Singleton;

public class TchatPreferencesHelper extends BasePreferencesHelper {

    private static final String SCREEN_NAME = "SCREEN_NAME";
    private static final String TCHAT_SCREEN_PAUSED = "TCHAT_SCREEN_PAUSED";
    private static final String TCHAT_THREAD_ID = "TCHAT_THREAD_ID";

    @Inject
    public TchatPreferencesHelper(@ApplicationContext Context context) {
        super(context);
    }

    public void saveScreenName(String screenName) {
        saveString(SCREEN_NAME, screenName);
    }

    public void saveScreenPaused(boolean isPaused) {
        saveBoolean(TCHAT_SCREEN_PAUSED, isPaused);
    }

    public void saveTchatThreadId(String threadId){
        saveString(TCHAT_THREAD_ID, threadId);
    }


    public String getTchatScreenName() {
        return getString(SCREEN_NAME);
    }

    public boolean isScreenPaused() {
        return getBoolean(TCHAT_SCREEN_PAUSED);
    }

    public String getTchatThreadId(){
        return getString(TCHAT_THREAD_ID);
    }


}
