package com.ose.data.local;

import com.ose.data.model.user.LoginAnswer;
import com.ose.injection.ApplicationContext;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class ConnexionPreferencesHelper extends BasePreferencesHelper {

    private static final String CONNEXION_INFORMATION = "CONNEXION_INFORMATION";

    @Inject
    public ConnexionPreferencesHelper(@ApplicationContext Context context) {
        super(context);
    }

    public void saveInformationConnexion(LoginAnswer loginAnswer) {
        saveJsonString(CONNEXION_INFORMATION, loginAnswer);
    }

    public LoginAnswer getInformationConnexion() {
        return (LoginAnswer) getJsonString(CONNEXION_INFORMATION, LoginAnswer.class);
    }

    public String getAccessToken() {
        return "Bearer " + getInformationConnexion().getAccessToken();
    }

    public void clearInformationConnexion() {
        removeJsonString(CONNEXION_INFORMATION);
    }
}
