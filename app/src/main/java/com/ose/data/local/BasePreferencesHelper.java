package com.ose.data.local;

import com.google.gson.Gson;

import com.ose.injection.ApplicationContext;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class BasePreferencesHelper {

    public static final String PREF_FILE_NAME = "ose_pref_file";

    protected final SharedPreferences mPref;

    @Inject
    public BasePreferencesHelper(@ApplicationContext Context context) {
        mPref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }

    public void clear() {
        mPref.edit().clear().apply();
    }

    protected void saveJsonString(String key, Object obj) {
        Gson gson = new Gson();
        String jsonInString = gson.toJson(obj);
        final SharedPreferences.Editor editor = mPref.edit();
        editor.putString(key, jsonInString);
        editor.apply();
    }

    protected Object getJsonString(String key, Class clazz) {
        final String serialized = mPref.getString(key, null);
        return (serialized == null) ? null : new Gson().fromJson(serialized, clazz);
    }

    protected void saveBoolean(String key, boolean bool) {
        final SharedPreferences.Editor editor = mPref.edit();
        editor.putBoolean(key, bool);
        editor.apply();
    }

    protected boolean getBoolean(String key, boolean defValue) {
        return mPref.getBoolean(key, defValue);
    }
    protected boolean getBoolean(String key) {
        return getBoolean(key, true);
    }

    protected void saveInteger(String key, int integer) {
        final SharedPreferences.Editor editor = mPref.edit();
        editor.putInt(key, integer);
        editor.apply();
    }

    protected int getInteger(String key) {
        return mPref.getInt(key, 0);
    }

    protected void saveString(String key, String string) {
        final SharedPreferences.Editor editor = mPref.edit();
        editor.putString(key, string);
        editor.apply();
    }

    protected String getString(String key) {
        return mPref.getString(key,  null);
    }



    protected void removeJsonString(String key) {
        final SharedPreferences.Editor editor = mPref.edit();
        editor.remove(key);
        editor.apply();
    }
}
