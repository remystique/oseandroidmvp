package com.ose.data;

import com.ose.data.local.ConnexionPreferencesHelper;
import com.ose.data.local.SettingsPreferencesHelper;
import com.ose.data.model.user.LoginAnswer;
import com.ose.data.model.user.RegisterAnswer;
import com.ose.data.model.user.User;
import com.ose.data.model.user.post.EmailAvailableBody;
import com.ose.data.model.user.post.LoginBody;
import com.ose.data.model.user.post.RegisterBody;
import com.ose.data.model.user.post.RegisterDeviceBody;
import com.ose.data.model.user.post.UpdateUserPictureBody;
import com.ose.data.remote.UserConnectedService;
import com.ose.data.remote.UserService;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Response;
import rx.Observable;

@Singleton
public class UserManager {

    private final UserService mUserService;
    private final UserConnectedService mUserConnectedService;

    private final ConnexionPreferencesHelper mConnexionPreferencesHelper;

    private final SettingsPreferencesHelper mSettingsPreferencesHelper;

    @Inject
    public UserManager(UserService userService,UserConnectedService userConnectedService, ConnexionPreferencesHelper connexionPreferencesHelper,
                       SettingsPreferencesHelper settingsPreferencesHelper) {
        mUserService = userService;
        mUserConnectedService = userConnectedService;
        mConnexionPreferencesHelper = connexionPreferencesHelper;
        mSettingsPreferencesHelper = settingsPreferencesHelper;
    }

    public Observable<RegisterAnswer> register(RegisterBody body) {
        return mUserService.register(body);
    }

    public Observable<Response<Void>> checkEmail(String email) {
        return mUserService.emailAvailable(EmailAvailableBody.builder().email(email).build());
    }

    public Observable<LoginAnswer> login(LoginBody body) {
        Map<String, String> data = new HashMap<>();
        data.put("grant_type", body.getGrantType());
        data.put("client_id", body.getClientId());
        data.put("client_secret", body.getClientSecret());
        data.put("username", body.getUsername());
        data.put("password", body.getPassword());
        return mUserService.login(data);
    }


    public Observable<User> user() {
        return mUserConnectedService.user();
    }

    public Observable<String> updateUser(RegisterBody registerBody) {
        return mUserConnectedService.updateUser(registerBody);
    }

    public Observable<String> updateUserPicture(String picture) {
        return mUserConnectedService
                .updateUserPicture(UpdateUserPictureBody.builder().encode(picture).build());
    }

    public void saveUserInformations(LoginAnswer loginAnswer) {
        mConnexionPreferencesHelper.saveInformationConnexion(loginAnswer);
    }

    public LoginAnswer getUserInformations() {
        return mConnexionPreferencesHelper.getInformationConnexion();
    }

    public void saveFirstTimeSetting(boolean isFirstTime) {
        mSettingsPreferencesHelper.saveFirstTimeSetting(isFirstTime);
    }

    public boolean getFirstTimeSetting() {
        return mSettingsPreferencesHelper.getFirstTimeSetting();
    }

    public Observable<Response<Void>> registerDevice(String tokenToRegister) {
        return mUserConnectedService.registerDevice(RegisterDeviceBody.builder().notificationToken(tokenToRegister).build());
    }

    public void clearUserInformations() {
        mConnexionPreferencesHelper.clearInformationConnexion();
    }
}
