package com.ose.data;

import com.ose.data.local.SettingsPreferencesHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class SettingsManager {

    private final SettingsPreferencesHelper mSettingsPreferencesHelper;

    @Inject
    public SettingsManager(SettingsPreferencesHelper settingsPreferencesHelper) {
        mSettingsPreferencesHelper = settingsPreferencesHelper;
    }

    public void saveMockSetting(boolean isMocked) {
        mSettingsPreferencesHelper.saveMockSetting(isMocked);
    }

    public boolean getMockSetting() {
        return mSettingsPreferencesHelper.getMockSetting();
    }

    public void saveMockDelaySetting(int mockDelay) {
        mSettingsPreferencesHelper.saveMockDelaySetting(mockDelay);
    }

    public int getMockDelaySetting() {
        return mSettingsPreferencesHelper.getMockDelaySetting();
    }
}
