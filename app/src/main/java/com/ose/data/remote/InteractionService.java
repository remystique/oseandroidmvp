package com.ose.data.remote;

import com.ose.data.model.interaction.ThreadDiscussionAnswer;
import com.ose.data.model.interaction.ThreadProfileAnswer;
import com.ose.data.model.interaction.ThreadsAnswer;
import com.ose.data.model.interaction.post.MessageBody;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

import static com.ose.data.remote.ServiceConstants.API_VERSION;

public interface InteractionService {

    @GET(API_VERSION + "interaction/thread")
    Observable<ThreadsAnswer> getThreads();

    @GET(API_VERSION + "interaction/message/{threadId}")
    Observable<ThreadDiscussionAnswer> getThreadDiscussion(@Path("threadId") String threadId);

    @GET(API_VERSION + "interaction/profile/{userId}")
    Observable<ThreadProfileAnswer> getThreadProfile(@Path("userId") String userId);

    @POST(API_VERSION + "interaction/message/{threadId}")
    Observable<Response<Void>> sendMessage(@Path("threadId") String threadId, @Body MessageBody messageBody);

    @POST(API_VERSION + "interaction/match/{userId}")
    Observable<String> matchUser(@Path("userId") String userId);

    @DELETE(API_VERSION + "interaction/match/{userId}")
    Observable<String> unmatchUser(@Path("userId") String userId);
}
