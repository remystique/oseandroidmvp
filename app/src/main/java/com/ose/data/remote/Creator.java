package com.ose.data.remote;

import com.google.gson.Gson;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.ose.BuildConfig;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.ose.data.remote.ServiceConstants.ENDPOINT;
import static com.ose.utils.GsonBuilder.getGson;

/**
 * ose
 *
 * Created by remybarbosa on 15/11/2017.
 */

public class Creator {

    public static Object newService(Class clazz) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG) {
            builder.addNetworkInterceptor(new StethoInterceptor());
        }
        builder.addInterceptor(chain -> {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("User-Agent", "android")
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        });
        OkHttpClient okHttpClient = builder.build();

        final Gson gson = getGson();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        return retrofit.create(clazz);
    }
}
