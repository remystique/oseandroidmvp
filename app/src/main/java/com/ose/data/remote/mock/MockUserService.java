package com.ose.data.remote.mock;

import com.ose.data.SettingsManager;
import com.ose.data.model.user.LoginAnswer;
import com.ose.data.model.user.RegisterAnswer;
import com.ose.data.model.user.post.EmailAvailableBody;
import com.ose.data.model.user.post.RegisterBody;
import com.ose.data.remote.UserService;

import android.content.Context;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import rx.Observable;

/**
 * ose
 * <p>
 * Created by remybarbosa on 13/11/2016.
 */
public class MockUserService implements UserService {

    public static final String TAG = MockUserService.class.getSimpleName();

    private final SettingsManager mSettingsManager;
    private Context mContext;

    public MockUserService(Context context, SettingsManager settingsManager) {
        mContext = context;
        mSettingsManager = settingsManager;
    }

    @Override
    public Observable<RegisterAnswer> register(@Body RegisterBody body) {
        MockObservableManager<RegisterAnswer> mockObservableManager = new MockObservableManager<>(mContext, mSettingsManager);
        return mockObservableManager.getObservable("model/user/JsonRegister.json", RegisterAnswer.class);
    }

    @Override
    public Observable<Response<Void>> emailAvailable(@Body EmailAvailableBody body) {
        MockObservableManager<Response<Void>> mockObservableManager = new MockObservableManager<>(mContext, mSettingsManager);
        String fileName = "model/user/JsonEmailAvailable.json";
        if (body.getEmail().equals("r@r.com")) {
            fileName = "model/user/JsonEmailNotAvailable.json";
        }
        return null;
    }

    @Override
    public Observable<LoginAnswer> login(@Body @FieldMap Map<String, String> data) {
        MockObservableManager<LoginAnswer> mockObservableManager = new MockObservableManager<>(mContext, mSettingsManager);
        return mockObservableManager.getObservable("model/user/JsonLogin.json", LoginAnswer.class);
    }

    @Override
    public Call<LoginAnswer> refreshToken(Map<String, String> data) {
        return null;
    }

}
