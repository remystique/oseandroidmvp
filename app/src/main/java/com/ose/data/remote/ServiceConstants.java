package com.ose.data.remote;

/**
 * ose
 * <p>
 * Created by remybarbosa on 14/01/2017.
 */
class ServiceConstants {

    static final String ENDPOINT = "https://ose-api.westeurope.cloudapp.azure.com:8443/ose/";

    static final String ENDPOINT_FRED = "http://priv-fred.freeboxos.fr/ose/";

    static final String API_VERSION = "";
}
