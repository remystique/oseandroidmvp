 package com.ose.data.remote;

import com.ose.data.model.user.LoginAnswer;
import com.ose.data.model.user.RegisterAnswer;
import com.ose.data.model.user.User;
import com.ose.data.model.user.post.EmailAvailableBody;
import com.ose.data.model.user.post.RegisterBody;
import com.ose.data.model.user.post.RegisterDeviceBody;
import com.ose.data.model.user.post.UpdateUserPictureBody;

import java.util.Map;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import rx.Observable;

import static com.ose.data.remote.ServiceConstants.API_VERSION;

 public interface UserConnectedService {

     @GET(API_VERSION + "user")
     Observable<User> user();

     @POST(API_VERSION + "user/register_device")
     Observable<Response<Void>> registerDevice(@Body RegisterDeviceBody registerDeviceBody);

     @PUT(API_VERSION + "user")
     Observable<String> updateUser(@Body RegisterBody registerBody);

     @POST(API_VERSION + "user")
     Observable<String> updateUserPicture(@Body UpdateUserPictureBody updateUserPictureBody);
 }
