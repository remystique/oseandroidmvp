package com.ose.data.remote.mock;

import com.ose.data.SettingsManager;
import com.ose.data.model.interaction.ThreadDiscussionAnswer;
import com.ose.data.model.interaction.ThreadProfileAnswer;
import com.ose.data.model.interaction.ThreadsAnswer;
import com.ose.data.model.interaction.post.MessageBody;
import com.ose.data.remote.InteractionService;

import android.content.Context;

import retrofit2.Response;
import rx.Observable;

/**
 * ose
 * <p>
 * Created by remybarbosa on 13/11/2016.
 */
public class MockInteractionService implements InteractionService {

    public static final String TAG = MockInteractionService.class.getSimpleName();

    private final SettingsManager mSettingsManager;
    private Context mContext;

    public MockInteractionService(Context context, SettingsManager settingsManager) {
        mContext = context;
        mSettingsManager = settingsManager;
    }

    @Override
    public Observable<ThreadsAnswer> getThreads() {
        return null;
    }

    @Override
    public Observable<ThreadDiscussionAnswer> getThreadDiscussion( String threadId) {
        return null;
    }

    @Override
    public Observable<ThreadProfileAnswer> getThreadProfile( String userId) {
        return null;
    }

    @Override
    public Observable<Response<Void>> sendMessage( String threadId, MessageBody messageBody) {
        return null;
    }

    @Override
    public Observable<String> matchUser( String userId) {
        return null;
    }

    @Override
    public Observable<String> unmatchUser( String userId) {
        return null;
    }
}
