package com.ose.data.remote;

import com.ose.data.model.game.DailyQuestionsAnswer;
import com.ose.data.model.game.ProgressionAnswer;
import com.ose.data.model.game.post.AnswerQuestionBody;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import rx.Observable;

import static com.ose.data.remote.ServiceConstants.API_VERSION;

public interface GameService {

    @GET(API_VERSION + "game/question/daily")
    Observable<DailyQuestionsAnswer> getDailyQuestions();

    @POST(API_VERSION + "game/answer")
    Observable<ResponseBody> answerQuestions(@Body AnswerQuestionBody answerQuestions);

    @GET(API_VERSION + "game/question/progression")
    Observable<ProgressionAnswer> getProgression();
}
