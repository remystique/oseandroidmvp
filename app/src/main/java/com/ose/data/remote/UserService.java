 package com.ose.data.remote;

import com.ose.data.model.user.LoginAnswer;
import com.ose.data.model.user.RegisterAnswer;
import com.ose.data.model.user.User;
import com.ose.data.model.user.UserAnswer;
import com.ose.data.model.user.post.EmailAvailableBody;
import com.ose.data.model.user.post.RegisterBody;
import com.ose.data.model.user.post.RegisterDeviceBody;
import com.ose.data.model.user.post.UpdateUserPictureBody;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import rx.Observable;

import static com.ose.data.remote.ServiceConstants.API_VERSION;

public interface UserService {

    @POST(API_VERSION + "authentication/register")
    Observable<RegisterAnswer> register(@Body RegisterBody body);

    @POST(API_VERSION + "authentication/email_available")
    Observable<Response<Void>> emailAvailable(@Body EmailAvailableBody body);

    @FormUrlEncoded
    @POST(API_VERSION + "authentication/token")
    Observable<LoginAnswer> login(@FieldMap Map<String, String> data);

    @FormUrlEncoded
    @POST(API_VERSION + "authentication/token")
    Call<LoginAnswer> refreshToken(@FieldMap Map<String, String> data);

}
