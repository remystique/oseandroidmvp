package com.ose.data.remote.mock;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;

import com.ose.data.SettingsManager;

import org.joda.time.DateTime;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import rx.Observable;

/**
 * ose
 * <p>
 * Created by remybarbosa on 13/11/2016.
 */
@Singleton
public class MockObservableManager<T> {

    public static final String TAG = MockObservableManager.class.getSimpleName();

    private final Context mContext;

    private final SettingsManager mSettingsManager;

    public MockObservableManager(Context context, SettingsManager settingsManager) {
        mContext = context;
        mSettingsManager = settingsManager;
    }


    public Observable<T> getObservable(String fileName, Class<T> classOfT) {
        try {
            AssetManager assetManager = mContext.getAssets();
            InputStream ims = assetManager.open(fileName);

            Gson gson = new GsonBuilder().registerTypeAdapter(DateTime.class,
                    (JsonDeserializer<DateTime>) (json, typeOfT, context) -> new DateTime(json.getAsString()))
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();

            Reader reader = new InputStreamReader(ims);

            final T obj = gson.fromJson(reader, classOfT);

            return Observable.just(obj).delay(mSettingsManager.getMockDelaySetting(), TimeUnit.MILLISECONDS);
        } catch (IOException e) {
            Log.e(TAG, classOfT.toString() + " : json deleted");
        }
        return null;
    }
}
