package com.ose.data.remote;

import com.ose.data.UserManager;
import com.ose.data.local.ConnexionPreferencesHelper;
import com.ose.data.model.user.LoginAnswer;

import java.io.IOException;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

public class TokenAuthenticator implements Authenticator {


    private final ConnexionPreferencesHelper mConnexionPreferencesHelper;
    private final UserService mUserService;

    public TokenAuthenticator(ConnexionPreferencesHelper connexionPreferencesHelper) {
        mConnexionPreferencesHelper = connexionPreferencesHelper;
        mUserService = (UserService) Creator.newService(UserService.class);
    }

    @Override
    public Request authenticate(Route route, Response response) throws IOException {
        Map<String, String> data = new HashMap<>();
        data.put("grant_type", "refresh_token");
        data.put("client_id", "ose-app");
        data.put("client_secret", "a455c1deccffda4fac015a8e9327e46e");
        data.put("refresh_token", mConnexionPreferencesHelper.getInformationConnexion().getRefreshToken());
        retrofit2.Response<LoginAnswer> loginAnswer = mUserService.refreshToken(data).execute();
        mConnexionPreferencesHelper.saveInformationConnexion(loginAnswer.body());

        return response.request().newBuilder()
                .header("Authorization", "Bearer " + loginAnswer.body().getAccessToken())
                .build();
    }


}