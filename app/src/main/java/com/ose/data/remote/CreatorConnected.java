package com.ose.data.remote;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.ose.BuildConfig;
import com.ose.data.local.ConnexionPreferencesHelper;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.ose.data.remote.ServiceConstants.ENDPOINT;
import static com.ose.utils.GsonBuilder.getGson;

/**
 * ose
 *
 * Created by remybarbosa on 15/11/2017.
 */

public class CreatorConnected {

    public static Object newService(Class clazz, ConnexionPreferencesHelper connexionPreferencesHelper) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS);
        if (BuildConfig.DEBUG) {
            builder.addNetworkInterceptor(new StethoInterceptor());
        }
        builder.authenticator(new TokenAuthenticator(connexionPreferencesHelper));
        builder.addInterceptor(chain -> {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("User-Agent", "android")
                    .header("Authorization", connexionPreferencesHelper.getAccessToken())
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        });
        OkHttpClient okHttpClient = builder.build();

        final Gson gson = getGson();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        return retrofit.create(clazz);
    }
}
