package com.ose.data;

import com.ose.data.local.SettingsPreferencesHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class OnBoardingManager {

    private final SettingsPreferencesHelper mSettingsPreferencesHelper;

    @Inject
    public OnBoardingManager(SettingsPreferencesHelper settingsPreferencesHelper) {
        mSettingsPreferencesHelper = settingsPreferencesHelper;
    }

    public void saveFirstTimeSetting(boolean isFirstTime) {
        mSettingsPreferencesHelper.saveFirstTimeSetting(isFirstTime);
    }
}
