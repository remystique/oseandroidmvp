package com.ose.data;

import com.ose.data.local.TchatPreferencesHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class BaseActivityManager {

    private final TchatPreferencesHelper mTchatPreferencesHelper;

    @Inject
    public BaseActivityManager(TchatPreferencesHelper tchatPreferencesHelper) {
        mTchatPreferencesHelper = tchatPreferencesHelper;
    }

    public void saveScreenName(String screenName) {
        mTchatPreferencesHelper.saveScreenName(screenName);
    }

    public void saveScreenPaused(boolean isPaused) {
        mTchatPreferencesHelper.saveScreenPaused(isPaused);
    }
}
