package com.ose.data.model.user.post;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 02/11/2016.
 */
@Builder
@Value
public class LoginBody {

    public static final String TAG = LoginBody.class.getSimpleName();

    private String grantType;
    private String clientId;
    private String clientSecret;
    private String username;
    private String password;
    private String refreshToken;
}
