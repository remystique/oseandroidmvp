package com.ose.data.model.interaction.view;

import com.ose.data.model.interaction.Message;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 23/01/2017.
 */
@Value
@Builder
public class MessageView {
    @MessageKind
    private int kind;
    private String title;
    private Message message;

    @MessageKind
    public int getKind() {
        return kind;
    }
}
