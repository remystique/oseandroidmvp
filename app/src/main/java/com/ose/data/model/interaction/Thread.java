package com.ose.data.model.interaction;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 23/01/2017.
 */
@Value
@Builder(toBuilder = true)
public class Thread implements Serializable {
    private String id;
    private Relation me;
    private Relation other;
    @SerializedName("last_message")
    private Message lastMessage;
}
