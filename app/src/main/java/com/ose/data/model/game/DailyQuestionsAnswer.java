package com.ose.data.model.game;

import java.util.List;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 14/01/2017.
 */
@Builder
@Value
public class DailyQuestionsAnswer {
    List<Question> data;
}
