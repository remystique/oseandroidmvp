package com.ose.data.model.game;

import java.util.List;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 27/02/2017.
 */
@Value
@Builder
public class ProgressionAnswer {
    List<Integer> data;
}
