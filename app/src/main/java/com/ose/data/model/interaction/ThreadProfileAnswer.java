package com.ose.data.model.interaction;


import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 23/01/2017.
 */
@Value
@Builder
public class ThreadProfileAnswer {
    private ThreadProfile data;
}
