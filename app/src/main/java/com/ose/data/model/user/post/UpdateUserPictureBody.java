package com.ose.data.model.user.post;

import java.io.Serializable;

import lombok.Builder;
import lombok.Value;

/**
 * Example
 * <p>
 * Created by remybarbosa on 17/05/16.
 */
@Builder(toBuilder = true)
@Value
public class UpdateUserPictureBody implements Serializable {
    private String encode;
}
