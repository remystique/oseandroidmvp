package com.ose.data.model.interaction;

import com.google.gson.annotations.SerializedName;
import com.ose.data.model.stringdef.RelationStatus;

import java.io.Serializable;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 23/01/2017.
 */

@Value
@Builder(toBuilder = true)
public class Relation implements Serializable {
    @SerializedName("user_id")
    private String id;
    @SerializedName("firstname")
    private String firstName;
    @RelationStatus
    private String status;
    private String picture;

    @RelationStatus
    public String getStatus() {
        return status;
    }
}
