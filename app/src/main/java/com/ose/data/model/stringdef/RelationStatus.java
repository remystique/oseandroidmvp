package com.ose.data.model.stringdef;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.ose.data.model.stringdef.RelationStatus.MATCHED;
import static com.ose.data.model.stringdef.RelationStatus.UNKNOWN;
import static com.ose.data.model.stringdef.RelationStatus.UNMATCHED;

@Retention(RetentionPolicy.SOURCE)
@StringDef({MATCHED, UNMATCHED, UNKNOWN})
public @interface RelationStatus {
    String MATCHED = "matched";
    String UNMATCHED = "unmatched";
    String UNKNOWN = "not matched";
}