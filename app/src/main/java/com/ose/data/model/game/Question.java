package com.ose.data.model.game;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 14/01/2017.
 */
@Builder
@Value
public class Question implements Serializable {
    ArrayList<Answer> answers;
    private String slug;

    public boolean equals(Object o) {
        final Question question = (Question) o;
        if (question == null || question.getSlug() == null) {
            return false;
        }
        return question.getSlug().equals(getSlug());
    }
}
