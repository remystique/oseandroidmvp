package com.ose.data.model.stringdef;

import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.ose.data.model.stringdef.NotificationScreen.CONVERSATION;

@Retention(RetentionPolicy.SOURCE)
@StringDef({CONVERSATION})
public @interface NotificationScreen {
    String CONVERSATION = "conversation";
}