package com.ose.data.model.interaction;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

import java.io.Serializable;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 23/01/2017.
 */
@Value
@Builder
public class Message implements Serializable {
    private int id;
    private String content;
    private DateTime date;
    @SerializedName("is_owner")
    private boolean isOwner;
}
