package com.ose.data.model.interaction;


import com.google.gson.annotations.SerializedName;
import com.ose.data.model.stringdef.RelationStatus;

import org.joda.time.DateTime;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 23/01/2017.
 */
@Value
@Builder
public class ThreadProfile {
    @SerializedName("user_id")
    private String userId;
    @SerializedName("first_name")
    private String firstName;
    @RelationStatus
    private String status;
    private String picture;
    private String city;
    @SerializedName("birth_date")
    private String birthDate;
}
