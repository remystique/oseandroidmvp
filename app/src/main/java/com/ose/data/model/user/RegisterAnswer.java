package com.ose.data.model.user;

import lombok.Builder;
import lombok.Value;

/**
 * Example
 * <p>
 * Created by remybarbosa on 17/05/16.
 */
@Builder
@Value
public class RegisterAnswer {
    private String password;
}
