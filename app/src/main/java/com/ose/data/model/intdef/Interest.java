package com.ose.data.model.intdef;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.ose.data.model.intdef.Interest.BOTH;
import static com.ose.data.model.intdef.Interest.MAN;
import static com.ose.data.model.intdef.Interest.WOMAN;

@Retention(RetentionPolicy.SOURCE)
@IntDef({MAN, WOMAN, BOTH})
public @interface Interest {
    int MAN = 0;
    int WOMAN = 1;
    int BOTH = 2;
}