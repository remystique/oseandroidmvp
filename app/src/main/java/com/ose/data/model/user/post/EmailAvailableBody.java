package com.ose.data.model.user.post;

import lombok.Builder;
import lombok.Value;

/**
 * Ose
 * <p>
 * Created by remybarbosa on 17/05/16.
 */
@Builder
@Value
public class EmailAvailableBody {
    private String email;
}
