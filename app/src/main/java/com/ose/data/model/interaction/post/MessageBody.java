package com.ose.data.model.interaction.post;

import java.io.Serializable;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 23/01/2017.
 */
@Value
@Builder
public class MessageBody implements Serializable {
    private String type;
    private String content;
}
