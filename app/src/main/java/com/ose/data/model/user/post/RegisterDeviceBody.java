package com.ose.data.model.user.post;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Builder;
import lombok.Value;

/**
 * Example
 *
 * Created by remybarbosa on 17/05/16.
 */
@Builder(toBuilder = true)
@Value
public class RegisterDeviceBody implements Serializable {
    @SerializedName("notification_token")
    private String notificationToken;
}
