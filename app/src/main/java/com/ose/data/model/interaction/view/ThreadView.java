package com.ose.data.model.interaction.view;

import com.ose.data.model.interaction.Thread;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 23/01/2017.
 */
@Value
@Builder(toBuilder = true)
public class ThreadView {
    @ThreadKind
    private int kind;
    private String title;
    private Thread thread;

    @ThreadKind
    public int getKind() {
        return kind;
    }
}
