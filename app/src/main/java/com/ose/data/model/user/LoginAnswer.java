package com.ose.data.model.user;

import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Value;

/**
 * Example
 * <p>
 * Created by remybarbosa on 17/05/16.
 */
@Builder
@Value
public class LoginAnswer {
    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("expires_in")
    private long expiresIn;
    @SerializedName("token_type")
    private String tokenType;
    private String scope;
    @SerializedName("refresh_token")
    private String refreshToken;
}
