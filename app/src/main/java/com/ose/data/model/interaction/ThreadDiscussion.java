package com.ose.data.model.interaction;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 23/01/2017.
 */
@Value
@Builder
public class ThreadDiscussion {
    @SerializedName("user_id")
    private String userId;
    @SerializedName("first_name")
    private String firstName;
    private String picture;
    private List<Message> messages;
}