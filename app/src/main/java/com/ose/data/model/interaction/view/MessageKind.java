package com.ose.data.model.interaction.view;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.ose.data.model.interaction.view.MessageKind.OTHER_MESSAGE;
import static com.ose.data.model.interaction.view.MessageKind.OWNER_MESSAGE;
import static com.ose.data.model.interaction.view.MessageKind.TITLE;

@Retention(RetentionPolicy.SOURCE)
@IntDef({TITLE, OTHER_MESSAGE, OWNER_MESSAGE})
public @interface MessageKind {
    int TITLE = 0;
    int OTHER_MESSAGE = 1;
    int OWNER_MESSAGE = 2;
}