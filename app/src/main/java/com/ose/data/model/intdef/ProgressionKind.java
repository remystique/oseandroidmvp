package com.ose.data.model.intdef;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.ose.data.model.intdef.ProgressionKind.ANSWERED;
import static com.ose.data.model.intdef.ProgressionKind.CANT_ANSWERED;
import static com.ose.data.model.intdef.ProgressionKind.NOT_ANSWERED;

@Retention(RetentionPolicy.SOURCE)
@IntDef({ANSWERED, CANT_ANSWERED, NOT_ANSWERED})
public @interface ProgressionKind {
    int ANSWERED = 0;
    int CANT_ANSWERED = 1;
    int NOT_ANSWERED = 2;
}