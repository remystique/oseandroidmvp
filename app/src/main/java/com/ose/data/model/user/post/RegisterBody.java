package com.ose.data.model.user.post;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;

import java.io.Serializable;

import lombok.Builder;
import lombok.Value;

/**
 * Example
 *
 * Created by remybarbosa on 17/05/16.
 */
@Builder(toBuilder = true)
@Value
public class RegisterBody implements Serializable {
    private String email;

    @SerializedName("firstname")
    private String firstName;

    @SerializedName("lastname")
    private String lastName;
    @SerializedName("birth_date")
    private String birthDate;
    private int gender;
    private int interest;
    private double lat;
    private double lng;

    private String city;
    private String picture;
    private String password;

}
