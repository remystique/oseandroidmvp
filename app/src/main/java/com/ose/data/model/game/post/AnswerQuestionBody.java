package com.ose.data.model.game.post;

import com.ose.data.model.game.Answer;

import java.util.List;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 14/01/2017.
 */
@Builder
@Value
public class AnswerQuestionBody {
    private List<AnswerQuestion> answers;
}
