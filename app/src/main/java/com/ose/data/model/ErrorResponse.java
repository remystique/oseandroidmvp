package com.ose.data.model;

import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 *
 * Created by remybarbosa on 30/10/2017.
 */

@Builder
@Value
public class ErrorResponse {

    @SerializedName("content")
    private String content;

}

