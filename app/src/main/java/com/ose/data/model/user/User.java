package com.ose.data.model.user;

import com.google.gson.annotations.SerializedName;

import com.ose.data.model.intdef.Gender;
import com.ose.data.model.intdef.Interest;

import org.joda.time.DateTime;

import java.io.Serializable;

import lombok.Builder;
import lombok.Value;

/**
 * Created by remybarbosa on 17/05/16.
 */
@Builder(toBuilder = true)
@Value
public class User implements Serializable {
    @SerializedName("user_id")
    private String userId;
    private String picture;
    @SerializedName("firstname")
    private String firstName;
    @SerializedName("lastname")
    private String lastName;
    private String city;
    @Gender
    private int gender;
    private double lat;
    private double lng;
    @Interest
    private int interest;
    @SerializedName("birth_date")
    private String birthDate;

    @Gender
    public int getGender() {
        return gender;
    }

    @Interest
    public int getInterest() {
        return interest;
    }
}
