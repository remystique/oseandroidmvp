package com.ose.data.model.game;

import java.io.Serializable;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 14/01/2017.
 */
@Builder
@Value
@EqualsAndHashCode
public class Answer implements Serializable {

    private int key;
    private String content;
}
