package com.ose.data.model.interaction;

import com.google.gson.annotations.SerializedName;
import com.ose.data.model.stringdef.NotificationScreen;

import java.io.Serializable;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 23/01/2017.
 */
@Value
@Builder
public class NotificationConversationContent implements Serializable {
    @SerializedName("thread_id")
    private String threadId;
}
