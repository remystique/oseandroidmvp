package com.ose.data.model.intdef;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.ose.data.model.intdef.Gender.FEMALE;
import static com.ose.data.model.intdef.Gender.MALE;

@Retention(RetentionPolicy.SOURCE)
@IntDef({MALE, FEMALE})
public @interface Gender {
    int MALE = 0;
    int FEMALE = 1;
}