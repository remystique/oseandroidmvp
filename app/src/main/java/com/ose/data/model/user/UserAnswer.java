package com.ose.data.model.user;

import java.io.Serializable;

import lombok.Builder;
import lombok.Value;

/**
 * Created by remybarbosa on 17/05/16.
 */
@Builder
@Value
public class UserAnswer implements Serializable {
    private User data;
}
