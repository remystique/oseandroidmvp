package com.ose.data.model.interaction.view;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.ose.data.model.interaction.view.ThreadKind.MATCHED;
import static com.ose.data.model.interaction.view.ThreadKind.TITLE;
import static com.ose.data.model.interaction.view.ThreadKind.UNKNOWN;
import static com.ose.data.model.interaction.view.ThreadKind.UNMATCHED;

@Retention(RetentionPolicy.SOURCE)
@IntDef({TITLE, UNKNOWN, MATCHED, UNMATCHED})
public @interface ThreadKind {
    int TITLE = 0;
    int UNKNOWN = 1;
    int MATCHED = 2;
    int UNMATCHED = 3;
}