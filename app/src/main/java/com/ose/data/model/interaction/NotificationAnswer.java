package com.ose.data.model.interaction;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Builder;
import lombok.Value;

/**
 * ose
 * <p>
 * Created by remybarbosa on 23/01/2017.
 */
@Value
@Builder
public class NotificationAnswer implements Serializable {
    @SerializedName("alert_title")
    private String alertTitle;
    @SerializedName("alert_content")
    private String alertContent;
    private NotificationInfo info;
}
