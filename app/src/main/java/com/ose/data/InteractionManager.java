package com.ose.data;

import com.ose.data.local.ConnexionPreferencesHelper;
import com.ose.data.local.TchatPreferencesHelper;
import com.ose.data.model.interaction.ThreadDiscussionAnswer;
import com.ose.data.model.interaction.ThreadProfileAnswer;
import com.ose.data.model.interaction.ThreadsAnswer;
import com.ose.data.model.interaction.post.MessageBody;
import com.ose.data.remote.InteractionService;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Response;
import rx.Observable;

@Singleton
public class InteractionManager {

    private final InteractionService mInteractionService;

    private final ConnexionPreferencesHelper mConnexionPreferencesHelper;

    private final TchatPreferencesHelper mTchatPreferencesHelper;

    @Inject
    public InteractionManager(InteractionService interactionService, ConnexionPreferencesHelper connexionPreferencesHelper, TchatPreferencesHelper tchatPreferencesHelper) {
        mInteractionService = interactionService;
        mConnexionPreferencesHelper = connexionPreferencesHelper;
        mTchatPreferencesHelper = tchatPreferencesHelper;
    }

    public Observable<ThreadsAnswer> getThreads() {
        return mInteractionService.getThreads();
    }

    public Observable<ThreadDiscussionAnswer> getThreadDiscussion(String threadId) {
        return mInteractionService.getThreadDiscussion( threadId);
    }

    public Observable<ThreadProfileAnswer> getThreadProfile(String userId) {
        return mInteractionService.getThreadProfile( userId);
    }

    public Observable<Response<Void>> sendMessage(String threadId, String message) {
        return mInteractionService.sendMessage( threadId,
                MessageBody.builder().content(message).type("text").build());
    }

    public Observable<String> matchUser(String userId) {
        return mInteractionService.matchUser( userId);
    }

    public Observable<String> unmatchUser(String userId) {
        return mInteractionService.unmatchUser( userId);
    }

    public void saveTchatThreadId(String threadId) {
        mTchatPreferencesHelper.saveTchatThreadId(threadId);
    }
}
