package com.ose.data;

import com.ose.data.local.ConnexionPreferencesHelper;
import com.ose.data.local.TchatPreferencesHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ErrorManager {

    private final ConnexionPreferencesHelper mConnexionPreferencesHelper;

    @Inject
    public ErrorManager(ConnexionPreferencesHelper connexionPreferencesHelper) {
        mConnexionPreferencesHelper = connexionPreferencesHelper;
    }

    public void clearUserInformations() {
        mConnexionPreferencesHelper.clearInformationConnexion();
    }
}
